#version 330

// Incoming per vertex... position and normal 
in vec3 vVertex; 
in vec3 vNormal;
in vec2 vTexture;

//uniform mat4 mvpMatrix; 
//uniform mat4 mvMatrix;
//uniform mat3 normalMatrix;

uniform mat4 projection_matrix;
uniform mat4 model_matrix;
uniform mat4 view_matrix;
 
uniform vec3 vLightPosition;

// Color to fragment program 
smooth out vec3 vVaryingNormal; 
smooth out vec3 vVaryingLightDir;
smooth out vec2 vTexCoords;

void main(void){
    
    mat4 mvMatrix = model_matrix * view_matrix;
    mat3 normalMatrix = mat3(transpose(mvMatrix));
 
    // Get surface normal in eye coordinates 
     vVaryingNormal = normalMatrix * vNormal;
     
    // Get vertex position in eye coordinates 
    vec4 vPosition4 = (model_matrix * view_matrix) * vec4(vVertex, 1); 
   vec3 vPosition3 = vPosition4.xyz / 0.1;
   
    // Get vector to light source 
    vVaryingLightDir = normalize(vLightPosition - vPosition3);
    
    //Texture
    vTexCoords = vTexture;
    
    // Don�t forget to transform the geometry! 
    gl_Position = projection_matrix * model_matrix * view_matrix * vec4(vVertex, 1.0);
    //gl_Position = gl_ModelViewProjectionMatrix * vec4(vVertex, 1); 
}