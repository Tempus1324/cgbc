#version 130

uniform sampler2D texture_diffuse;

in vec4 color; //The color passed in from the vertex shader (interpolated)
//in vec2 pass_TextureCoord;
out vec4 outColor; //Define the output of our fragment shader

void main(void)
{
    //outColor = color; //Copy the color to the output
    outColor = texture2D(texture_diffuse, pass_TextureCoord);
}