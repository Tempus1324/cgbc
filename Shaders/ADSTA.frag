#version 330 core
#extension GL_EXT_texture_array : enable

out vec4 vFragColor;

uniform sampler2DArray sampler;
uniform float textureIndex;

uniform vec4 ambientColor; 
uniform vec4 diffuseColor; 
uniform vec4 specularColor;
uniform float shine;

uniform vec3 lightAmbient;
uniform vec3 lightDiffuse;
uniform vec3 lightSpecular;

in vec3 vVaryingNormal; 
in vec3 vVaryingLightDir;

smooth in vec2 vTexCoords;

void main(void){ 
    // Dot product gives us diffuse intensity 
    float diff = max(0.0, dot(normalize(vVaryingNormal), normalize(vVaryingLightDir)));
    
    // Multiply intensity by diffuse color, force alpha to 1.0 
    vFragColor = diff * diffuseColor;
    
    // Add in ambient light 
    vFragColor += ambientColor;
    
    //Add in texture colour
    vFragColor = texture2DArray(sampler, vec3(vTexCoords, textureIndex));
    
    // Specular Light 
    vec3 vReflection = normalize(reflect(-normalize(vVaryingLightDir), normalize(vVaryingNormal))); 
    float spec = max(0.0, dot(normalize(vVaryingNormal), vReflection));
    
    // If the diffuse light is zero, don�t even bother with the pow function 
    if(diff != 0) {
        float fSpec = pow(spec, 128.0); 
        //vFragColor.rgb += vec3(fSpec, fSpec, fSpec); 
    }
   //vFragColor = vec4(0, 1, 1, 1);
}