#version 130

uniform mat4 projection_matrix;
uniform mat4 model_matrix;
uniform mat4 view_matrix;

in vec3 in_Position;
in vec3 in_Colour; //The color attribute which was passed in from the program
in vec2 in_TextureCoord;

out vec4 color; //The output color which will be passed to the fragment shader
out vec2 pass_TextureCoord;

void main(void) {
    
   //gl_Position = gl_ModelViewProjectionMatrix * vec4(in_Position, 1.0);
   //gl_Position = vec4(in_Position, 0);
   gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_Position, 1.0);
    color = vec4(1,1,1, 1.0);
    pass_TextureCoord = in_TextureCoord;
}