package me.tempus.util;

import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class ModelLoaderold {


	public static ImpModel loadModel(File modelFile) throws IOException{

		BufferedReader reader = new BufferedReader(new FileReader(modelFile));
		BufferedReader readerMtl = null;
		ImpModel model = new ImpModel();
		String currentLine;

		/**
		 * TODO Add Texture vertices (vt)
		 * TODO Add material (lighting)
		 * 
		 */
		String materialName = null;

		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector3f> normals = new ArrayList<Vector3f>();
		List<Vector2f> textureUVs = new ArrayList<Vector2f>();
		List<String> faces = new ArrayList<String>();
		
		HashMap<String, Integer> loadedVertices = new HashMap<String, Integer>();
		

		while((currentLine = reader.readLine()) != null){			

			if(currentLine.startsWith("mtllib ")){
				String[] v = currentLine.split(" ");
				readerMtl = new BufferedReader(new FileReader("Data" + File.separator + v[1]));
			}else if(currentLine.startsWith("usemtl ")){
				String[] v = currentLine.split(" ");
				materialName = v[1];
			}else if(currentLine.startsWith("vt ")){
				String[] v = currentLine.split(" ");
				float x = Float.valueOf(v[1]);
				float y = Float.valueOf(v[2]);
				textureUVs.add(new Vector2f(x, y));
			}else if(currentLine.startsWith("v ")){
				String[] v = currentLine.split(" ");
				float x = Float.valueOf(v[1]);
				float y = Float.valueOf(v[2]);
				float z = Float.valueOf(v[3]);
				vertices.add(new Vector3f(x, y, z));
			}else if(currentLine.startsWith("vn ")){
				String[] v = currentLine.split(" ");
				float x = Float.valueOf(v[1]);
				float y = Float.valueOf(v[2]);
				float z = Float.valueOf(v[3]);
				normals.add(new Vector3f(x,y,z));
			}else if(currentLine.startsWith("f ")){
				try{
					String[] v = currentLine.split(" ");
					//System.out.println(v[0] + " "  + v[1] + " "  + v[2] + " " + v[3]);
					String[] c1 = v[1].split("/");
					String[] c2 = v[2].split("/");
					String[] c3 = v[3].split("/");

					Vector3f vertextIndices = new Vector3f(
							Float.valueOf(c1[0]), Float.valueOf(c2[0]), Float.valueOf(c3[0]));
					Vector3f textureIndices = new Vector3f(
							Float.valueOf(c1[1]), Float.valueOf(c2[1]), Float.valueOf(c3[1]));
					Vector3f normalIndices = new Vector3f(
							Float.valueOf(c1[2]), Float.valueOf(c2[2]), Float.valueOf(c3[2]));
					//faces.add(new Face(vertextIndices, textureIndices, normalIndices, materialName));
					
					StringBuilder line = new StringBuilder();
					line.append(c1[0]).append(c1[1]).append(c1[2]);
					line.append(c2[0]).append(c2[1]).append(c2[2]);
					line.append(c3[0]).append(c3[1]).append(c3[2]);
					line.trimToSize();
					
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}
		if(readerMtl != null){
			while((currentLine = readerMtl.readLine()) != null){
				float[] ambient = new float[3];
				float[] diffuse = new float[3];
				float[] spectual = new float[3];
				float shine = 0;
				String materialName1 = null;

				if(currentLine.startsWith("Ka ")){
					String[] v = currentLine.split(" ");
					ambient[0] = Float.parseFloat(v[1]);
					ambient[1] = Float.parseFloat(v[2]);
					ambient[2] = Float.parseFloat(v[3]);
				}else if(currentLine.startsWith("Kd ")){
					String[] v = currentLine.split(" ");
					diffuse[0] = Float.parseFloat(v[1]);
					diffuse[1] = Float.parseFloat(v[2]);
					diffuse[2] = Float.parseFloat(v[3]);
				}else if(currentLine.startsWith("Ks ")){
					String[] v = currentLine.split(" ");
					spectual[0] = Float.parseFloat(v[1]);
					spectual[1] = Float.parseFloat(v[2]);
					spectual[2] = Float.parseFloat(v[3]);
				}else if(currentLine.startsWith("Ns ")){
					String[] v = currentLine.split(" ");
					shine = Float.parseFloat(v[1]);
				}else if(currentLine.startsWith("newmtl ")){
					String[] v= currentLine.split(" ");
					materialName1 = v[1];
				}

				model.addMaterial(materialName1, new Material(ambient, diffuse, spectual, shine));
			}
		}
		reader.close();
		if(readerMtl != null){
			readerMtl.close();
		}
		System.out.println("Amount of faces: " + faces.size());
		System.out.println("Amount of UVs: " + textureUVs.size());
		System.out.println("Amount of normals: " + normals.size() *3);
		model.setFloatSizes(vertices.size() * 3 * faces.size(), normals.size() * 3 * faces.size(), textureUVs.size() * 2 * faces.size());
		
		for (Face face : faces) {

			Vector3f n1 = normals.get((int) face.getNormal().x - 1);
			model.addNoraml(n1.x, n1.y, n1.z);
			
			Vector3f v1 = vertices.get((int) face.getVertex().x - 1);
			model.addVertex(v1.x, v1.y, v1.z);
			
			Vector2f t1 = textureUVs.get((int) face.getTexture().x - 1);
			model.addTextureUV(t1.x, t1.y);
			
			Vector3f n2 = normals.get((int) face.getNormal().y - 1);
			model.addNoraml(n2.x, n2.y, n2.z);
			
			Vector3f v2 = vertices.get((int) face.getVertex().y - 1);
			model.addVertex(v2.x, v2.y, v2.z);
			
			Vector2f t2 = textureUVs.get((int) face.getTexture().y - 1);
			model.addTextureUV(t2.x, t2.y);
			
			Vector3f n3 = normals.get((int) face.getNormal().z - 1);
			model.addNoraml(n3.x, n3.y, n3.z);
			
			Vector3f v3 = vertices.get((int) face.getVertex().z - 1);
			model.addVertex(v3.x, v3.y, v3.z);
			
			Vector2f t3 = textureUVs.get((int) face.getTexture().z - 1);
			model.addTextureUV(t3.x, t3.y);
		}

		return model;

	}

	public static ImpModel deserialModel(String modelPath){
		ImpModel model = null;


		try {
			FileInputStream fis = new FileInputStream(modelPath); 
			ObjectInputStream ois = new ObjectInputStream(fis);
			model = (ImpModel)ois.readObject();
			ois.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}


		return model;
	}

	/**
	 * Serialize a model
	 */
	public static void main(String[] args){
		ImpModel model;

		System.out.println("Please enter the name of the model file to be loaded (*.obj): ");
		Scanner scanner = new Scanner(System.in);
		String path = scanner.nextLine();		
		scanner.close();

		try {
			model = loadModel(new File("Data/" + path));
			System.out.println("Models" + File.separator + path);
			FileOutputStream fos = new FileOutputStream("Models" + File.separator + path); 
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(model);
			oos.flush();
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
