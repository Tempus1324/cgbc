/**
 * 
 */
package me.tempus.jobs;

/**
 * @author Chris
 *
 */
public abstract class GLJob {

	public GLJob() {
	}
	
	public abstract void executeTask();
	
	public abstract void initData(Object...objects);
	
	
}
