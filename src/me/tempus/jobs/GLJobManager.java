package me.tempus.jobs;

import java.util.ArrayList;

public class GLJobManager {

	private static ArrayList<GLJob> jobs = new ArrayList<GLJob>();

	public static void processJobs(){
		GLJob[] job = jobs .toArray(new GLJob[jobs.size()]);
		jobs.clear();
		jobs.trimToSize();
		for(GLJob currentJob : job){
			currentJob.executeTask();
		}
	}
	
	public static void addJob(GLJob job){
		jobs.add(job);
	}

	public static void addJobs(GLJob[] jobs2) {
		// TODO Auto-generated method stub
		for(GLJob job : jobs2){
			jobs.add(job);
		}
	}
	
}
