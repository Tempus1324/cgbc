/**
 * 
 */
package me.tempus.jobs;

import me.tempus.shader.Shader;

/**
 * @author Chris
 *
 */
public class GLJob_ShaderSetUp extends GLJob {

	private Shader shader;
	private GLJobi callerInstance;
	
	
	/**
	 * @param shader
	 */
	public GLJob_ShaderSetUp(Shader shader) {
		super();
		this.shader = shader;
	}

	/* (non-Javadoc)
	 * @see me.tempus.jobs.GLJob#executeTask()
	 */
	@Override
	public void executeTask() {
		shader.loadShaders();
		callerInstance.postResults(this, shader);
	}

	/* (non-Javadoc)
	 * @see me.tempus.jobs.GLJob#initData(java.lang.Object[])
	 */
	@Override
	public void initData(Object... objects) {
		

	}

}
