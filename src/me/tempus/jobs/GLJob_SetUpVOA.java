/**
 * 
 */
package me.tempus.jobs;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/**
 * @author Chris
 *
 */
public class GLJob_SetUpVOA extends GLJob {

	private List<FloatBuffer> floats;
	private int[] sizes;
	private IntBuffer indicesBuffer;
	private Integer id;
	private Integer indiciesID;
	private GLJobi interfaceCaller;

	
	
	/* (non-Javadoc)
	 * @see me.tempus.jobs.GLJob#executeTask(java.lang.Object[])
	 */
	@Override
	public void executeTask() {

		int vao = -1;
		int indicesID = -1;
		vao = GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vao);

		int floatByteSize = 4;
		int vbo1 = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo1);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, floats.get(0), GL15.GL_STATIC_DRAW); 



		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 12, 0);

		if(floats.size() >= 2){
			int vbo2 = GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo2);
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, floats.get(1), GL15.GL_STATIC_DRAW);

			GL20.glVertexAttribPointer(1, sizes[1], GL11.GL_FLOAT, false, 0, 0);
		}

		if(floats.size() >= 3){
			int vbo3 = GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo3);
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, floats.get(2), GL15.GL_STATIC_DRAW);

			GL20.glVertexAttribPointer(2, sizes[2], GL11.GL_FLOAT, false, 0, 0);
		}

		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL30.glBindVertexArray(0);

		indicesID = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesID);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);

		interfaceCaller.postResults(this, vao, indicesID);
	}

	/**
	 *
	 * @param floats
	 * @param sizes
	 * @param indicesBuffer
	 */
	public GLJob_SetUpVOA(GLJobi interfaceCaller, List<FloatBuffer> floats, int[] sizes,
			IntBuffer indicesBuffer) {
		super();
		this.floats = floats;
		this.sizes = sizes;
		this.indicesBuffer = indicesBuffer;
		this.interfaceCaller = interfaceCaller;
	}

	/**
 	 * Set List<FloatBuffer>, int[] sizes, IntBuffer indicesbuffer, Integer id, Integer indicesID
	 */
	@Override
	public void initData(Object... objects) {

		
		
	}

}


