/**
 * 
 */
package me.tempus.jobs;

import me.tempus.modelutil.ImpModel;
import me.tempus.modelutil.ModelLoaderTemplate;

import org.lwjgl.util.vector.Vector3f;

/**
 * @author Chris
 *
 */
public class GLJob_ModelSetUp extends GLJob {

	private String modelName;
	private GLJobi interfaceCaller;
	private Vector3f translate = new Vector3f(0,0,0);
	private Vector3f scale = new Vector3f(1,1,1);
	private Vector3f rotate = new Vector3f(0,0,0);
	
	
	/**
	 * @param modelName
	 * @param interfaceCaller
	 */
	public GLJob_ModelSetUp(String modelName, GLJobi interfaceCaller) {
		super();
		this.modelName = modelName;
		this.interfaceCaller = interfaceCaller;
	}

	/**
	 * @param modelName
	 * @param interfaceCaller
	 * @param translate
	 * @param scale
	 * @param rotate
	 */
	public GLJob_ModelSetUp(String modelName, GLJobi interfaceCaller,
			Vector3f translate, Vector3f scale, Vector3f rotate) {
		super();
		this.modelName = modelName;
		this.interfaceCaller = interfaceCaller;
		this.translate = translate;
		this.scale = scale;
		this.rotate = rotate;
	}



	/* (non-Javadoc)
	 * @see me.tempus.jobs.GLJob#executeTask()
	 */
	@Override
	public void executeTask() {
		
		final ImpModel model = ModelLoaderTemplate.deserialModel(modelName);
		model.setTranslate(translate);
		model.setRotate(rotate);
		model.setScale(scale);
		interfaceCaller.postResults(this, model);
	}

	/* (non-Javadoc)
	 * @see me.tempus.jobs.GLJob#initData(java.lang.Object[])
	 */
	@Override
	public void initData(Object... objects) {
		

	}

}
