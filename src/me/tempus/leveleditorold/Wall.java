/**
 * 
 */
package me.tempus.leveleditorold;


import me.tempus.modelutil.ImpModel;
import me.tempus.shader.Shader;

import org.lwjgl.util.vector.Vector3f;

/**
 * @author Chris
 *
 */
public class Wall extends LevelObject{

	private WallBatch batch;
	
	public Wall(int textureIndex, Vector3f translate, Vector3f rotation,
			Vector3f scale, WallBatch batch) {
		super(textureIndex, translate, rotation, scale);
		this.batch = batch;
		batch.addWall(this);
		// TODO Auto-generated constructor stub
	}
	
	public void remove(){
		batch.removeWall(this);
	}
	
}
