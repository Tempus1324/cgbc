/**
 * 
 */
package me.tempus.leveleditorold;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;

/**
 * @author Chris
 *
 */
public class WallGenerator {

	public WallGenerator(){
		
	}
	
	public static WallBatch generateWall(Vector3f scale, Vector3f rotation, float width, float height, float depth, int amountOfTiles){
		WallBatch walls = new WallBatch();
		int index = 0;
		float wallHeight = height/amountOfTiles;
		float wallWidth = width/amountOfTiles;
		float wallDepth = depth/amountOfTiles;
		for(int i = 0; i < amountOfTiles; i++){
			//for(int j = 0; j < 1; j++){
				index = 1 - index;
				walls.addWall(new Wall(index, new Vector3f(i * 1.99f, 0, 0), rotation, scale, walls));
			//}
		}
		
		return walls;
	}
	
}
