/**
 * 
 */
package me.tempus.leveleditorold;

import java.awt.EventQueue;
import java.io.File;

import me.tempus.levelediter.LevelWindow;
import me.tempus.modelutil.ADSTAModel;
import me.tempus.modelutil.ImpModel;
import me.tempus.modelutil.ModelLoader;
import me.tempus.shader.ADSShader;
import me.tempus.shader.ADSTAShader;
import me.tempus.shader.Shader;
import me.tempus.util.Camera3;
import me.tempus.util.PVM;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * @author Chris
 *
 */
public class LevelEditor {
	
	private boolean fullscreen = false;
	private String TITLE = "Level Editor";
	private int width = 1600;
	private int height = 900;
	private Shader shader;
	private boolean done;
	private ImpModel currentObject;
	private Walls wallManager;
	
	private Vector3f objectTranslate = new Vector3f(0, 0, 0);
	private Vector3f objectRotation = new Vector3f(0, 0, 0);
	private Vector3f objectScale = new Vector3f(1, 1, 1);
	
	private final ImpModel[] models = new ImpModel[4];
	private final Shader[] shaderID = new Shader[2];
	
	private int currentShader = 1;
	
	int index = 0;
	
	private ADSTAModel bike;
	
	/**
	 * TODO Make a new model class for each Shader
	 */
	
	public void run(){
		try{
			startEditer();
			createWindow();
			//setUpShaders();
			shader = new ADSTAShader("Shaders" + File.separatorChar + "ADSTA.vert", "Shaders" + File.separatorChar + "ADSTA.frag");
			shader.loadShaders();
			shaderID[0] = shader;
			shader = new ADSShader("Shaders" + File.separatorChar + "ADS.vert", "Shaders" + File.separatorChar + "ADS.frag");
			shader.loadShaders();
			((ADSShader)shader).setLightPosistion(new Vector3f(0,5,10));
			shaderID[1] = shader;
			init();

			Keyboard.enableRepeatEvents(true);
			
			while(!done){
				update();
				GL20.glUseProgram(shaderID[currentShader].getShaderID());
				render();
				
				Display.update();
				Display.sync(60);
				GL20.glUseProgram(0);
			}
			cleanup();
		}catch (Exception e){
			e.printStackTrace();
			System.exit(0);
		}
		System.exit(0);

	}
	
	private void startEditer() {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//LevelWindow window = new LevelWindow();
					//((LevelWindow) window).getFrmLevelEditer().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void render() {
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		// TODO Auto-generated method stub
		PVM.loadIdentity();
		Camera3.transform();
		//wallManager.drawWall(shaderID[currentShader]);
		for(ImpModel model : models){
			if(model != null)
				model.draw();
			
		}
		
	}

	private void update() {
		// TODO Auto-generated method stub
		Camera3.processAngles();
		Vector3f pos = Camera3.getCameraPos();
		while(Keyboard.next()){
			if(Keyboard.getEventKeyState()){
				switch(Keyboard.getEventKey()){				
				case Keyboard.KEY_W:
					Camera3.addZ(-1);
					Camera3.moveUp(1);
					break;
				case Keyboard.KEY_S:
					Camera3.addZ(1);
					Camera3.moveUp(-1);
					break;
				case Keyboard.KEY_A:
					Camera3.addX(-1);
					break;
				case Keyboard.KEY_D:
					Camera3.addX(1);
					break;
				case Keyboard.KEY_SPACE:
					Camera3.addY(1);
					break;
				case Keyboard.KEY_C:
					objectRotation = new Vector3f(90f, 0, 0);
					objectScale = new Vector3f(1f, 1f, 1f);
					//Walls.instance.addWallBatch(WallGenerator.generateWall(objectScale, objectRotation, 512, 512, 1, 15));
					index = 1 - index;
					break;
				case Keyboard.KEY_0:
					models[0] = ModelLoader.deserialModel("Models" + File.separator + "fhome.obj");
					pos.x = pos.x * -1;
					pos.y = pos.y * -1;
					pos.z = pos.z * -1;
					models[0].setTranslate(pos);
					break;
				case Keyboard.KEY_1:
					models[1] = ModelLoader.deserialModel("Models" + File.separator + "tool.obj");
					pos.x = pos.x * -1;
					pos.y = pos.y * -1;
					pos.z = pos.z * -1;
					models[1].setTranslate(pos);
					break;
				case Keyboard.KEY_2:
					models[2] = ModelLoader.deserialModel("Models" + File.separator + "lego.obj");
					pos.x = pos.x * -1;
					pos.y = pos.y * -1;
					pos.z = pos.z * -1;
					//models[2].setRotate(new Vector3f(0, -90, 0));
					models[2].setTranslate(pos);
					break;
				case Keyboard.KEY_3:
					break;
				case Keyboard.KEY_4:
					break;
				case Keyboard.KEY_5:
					currentShader = 1 - currentShader;
					break;
				case Keyboard.KEY_G:
					((ADSShader)shader).setLightPosistion(Camera3.getCameraPos());
					break;
				case Keyboard.KEY_RIGHT:
					objectTranslate.x += 0.1f;
					currentObject.setTranslate(objectTranslate);
					break;
				case Keyboard.KEY_LEFT:
					objectTranslate.x -= 0.1f;
					currentObject.setTranslate(objectTranslate);
					break;
				case Keyboard.KEY_R:
					Camera3.rotX(3);
					currentObject.setRotate(objectRotation);
					break;
				case Keyboard.KEY_UP:
					objectTranslate.z += 1;
					currentObject.setTranslate(objectTranslate);
					break;
				case Keyboard.KEY_DOWN:
					objectTranslate.z -= 1;
					currentObject.setTranslate(objectTranslate);
					break;
				}
			}
		}
		
	}

	public void init(){
		GL11.glEnable(GL11.GL_TEXTURE_2D);                          // Enable Texture Mapping
		GL11.glShadeModel(GL11.GL_SMOOTH);                          // Enables Smooth Color Shading
		GL11.glClearColor(0.5f, 0.5f, 0.5f, 0.0f);                // This Will Clear The Background Color To Black
		GL11.glClearDepth(1.0);                                   // Enables Clearing Of The Depth Buffer
		GL11.glEnable(GL11.GL_DEPTH_TEST);                          // Enables Depth Testing
		GL11.glDepthFunc(GL11.GL_LEQUAL);                           // The Type Of Depth Test To Do
		PVM.setUpProjection(45f, width, height, 0.1f, 100f);
		System.out.println("OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));
//		System.out.println("Extenstion String: " + GL11.glGetString(GL11.GL_EXTENSIONS));
		String extension = "GL_EXT_texture_array";
		if((GL11.glGetString(GL11.GL_EXTENSIONS)).contains(extension)){
			System.out.println(extension + " is an extension");
		}
		Camera3.setSpeed(5);
		
		//wallManager = new Walls();
		//currentObject = new Wall(0, objectTranslate, objectRotation, objectScale);
		//bike = ADSTAModel.phaseToThis(ModelLoader.deserialModel("Models" + File.separator + "square.obj"));
		//bike.setTranslate(new Vector3f(0,0,-5));
	}

	private void createWindow() throws Exception {
		PixelFormat pixelFormat = new PixelFormat();
		ContextAttribs contextAtrributes = new ContextAttribs(3, 3);
		contextAtrributes.withForwardCompatible(true);
		contextAtrributes.withProfileCore(true);

		Display.setFullscreen(fullscreen);
		Display.setDisplayMode(new DisplayMode(width, height));
		Display.setTitle(TITLE);
		//Display.create(pixelFormat, contextAtrributes);
		Display.create();
		GL11.glViewport(0, 0, width, height);
	}

	public void cleanup(){
		GL20.glDeleteProgram(shader.getShaderID());
		GL20.glDeleteShader(shader.getVertID());
		GL20.glDeleteShader(shader.getFragID());
		Display.destroy();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new LevelEditor().run();

	}

}
