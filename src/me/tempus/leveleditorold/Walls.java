/**
 * 
 */
package me.tempus.leveleditorold;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;

import me.tempus.modelutil.ADSTAModel;
import me.tempus.modelutil.ImpModel;
import me.tempus.modelutil.ModelLoader;
import me.tempus.shader.Shader;
import me.tempus.util.PVM;
import me.tempus.util.Utilities;

/**
 * @author Chris
 *
 */
public class Walls {

	private ADSTAModel wallModel;
	private List<WallBatch> walls = new ArrayList<WallBatch>();
	public static Walls instance;
	
	public Walls(){
		wallModel = ADSTAModel.phaseToThis(ModelLoader.deserialModel("Models" + File.separator + "wall.obj"));
		instance = this;

	}
	
	public void drawWall(Shader shader){
		for(WallBatch wall : walls){
			
			wall.drawWall(shader, wallModel);
		}
	}
	
	public void addWallBatch(WallBatch wall){
		walls.add(wall);
	}
	
	public void removeWallBatch(WallBatch wall){
		walls.remove(wall);
	}
}
