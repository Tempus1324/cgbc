package me.tempus.leveleditorold;

import org.lwjgl.util.vector.Vector3f;

import me.tempus.modelutil.ImpModel;


public class LevelObject {

	protected int textureIndex;
	protected Vector3f translate;
	protected Vector3f rotate;
	protected Vector3f scale;	
	
	public LevelObject(int textureIndex, Vector3f translate, Vector3f rotation,
			Vector3f scale) {
		
		this.textureIndex = textureIndex;
		this.translate = translate;
		this.rotate = rotation;
		this.scale = scale;
	}

	
	
	/**
	 * @param translate the translate to set
	 */
	public void setTranslate(Vector3f translate) {
		this.translate = translate;
	}



	/**
	 * @param rotate the rotate to set
	 */
	public void setRotate(Vector3f rotate) {
		this.rotate = rotate;
	}



	/**
	 * @param scale the scale to set
	 */
	public void setScale(Vector3f scale) {
		this.scale = scale;
	}



	public int getTextureIndex(){
		return textureIndex;
	}

	/**
	 * @return the translate
	 */
	public Vector3f getTranslate() {
		return translate;
	}

	/**
	 * @return the rotate
	 */
	public Vector3f getRotate() {
		return rotate;
	}

	/**
	 * @return the scale
	 */
	public Vector3f getScale() {
		return scale;
	}
	
}
