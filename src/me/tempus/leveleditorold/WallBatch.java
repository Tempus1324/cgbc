/**
 * 
 */
package me.tempus.leveleditorold;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;

import me.tempus.modelutil.ADSTAModel;
import me.tempus.modelutil.ImpModel;
import me.tempus.shader.Shader;
import me.tempus.util.PVM;
import me.tempus.util.Utilities;

/**
 * @author Chris
 *
 */
public class WallBatch {

	private List<Wall> walls = new ArrayList<Wall>();
	private Wall currentWall;
	private int textureArray;
	private Vector3f translate = new Vector3f(0, 0, 0);
	private Vector3f scale = new Vector3f(1, 1, 1);
	private Vector3f rotation = new Vector3f(0, 0, 0);
	
	public WallBatch(){
		String[] files = new String[2];
		files[0] = ("PlainConcreteWall");
		files[1] = ("ConcreteWall");
		textureArray = Utilities.loadTextureArray("Data" + File.separatorChar + "Measum", files, 255, 255, "JPG");
	}
	
	public void drawWall(Shader shader, ADSTAModel model){
		/**
		 * Binds texture
		 * Loops through the wall array
		 * Applies tranformations
		 * Lighting
		 * Pass other uniforms
		 * Calls VAO for the staple wall
		 */
		
		
		
		GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, textureArray);
		for(Wall currentWall : walls){
			PVM.pushMatrix();
			PVM.translate(translate.x, translate.y, translate.z);
			PVM.scale(scale.x, scale.y, scale.z);
			PVM.rotate(rotation.x, rotation.y, rotation.z);
			
			model.setTranslate(currentWall.getTranslate());
			model.setRotate(currentWall.getRotate());
			model.setScale(currentWall.getScale());
			model.setTextureIndex(currentWall.getTextureIndex());
			model.draw();
			PVM.popMatrix();
		}
		GL11.glBindTexture(0, textureArray);
		PVM.loadIdentity();
	}
	
	
	
	/**
	 * @param walls the walls to set
	 */
	public void setWalls(List<Wall> walls) {
		this.walls = walls;
	}

	/**
	 * @param translate the translate to set
	 */
	public void setTranslate(Vector3f translate) {
		this.translate = translate;
	}

	/**
	 * @param scale the scale to set
	 */
	public void setScale(Vector3f scale) {
		this.scale = scale;
	}

	/**
	 * @param rotation the rotation to set
	 */
	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
	}

	public void addWall(Wall wall){
		walls.add(wall);
	}
	
	public void removeWall(Wall wall){
		walls.remove(wall);
	}
}
