package me.tempus.levelediter;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.miginfocom.swing.MigLayout;

public class LevelWindow {

	private InputManager inputManager;

	private ArrayList<String> models = new ArrayList<String>();

	private String[] possibleModels;
	private String modelDir = "Models";

	private JFrame frmLevelEditer;
	private JTextField txtXTrans;
	private JTextField txtYTrans;
	private JTextField txtZTrans;
	private JLabel lblZ;
	private JButton btnTranslate;
	private JTextField txtXRot;
	private JTextField txtYRot;
	private JTextField txtZRot;
	private JLabel label;
	private JLabel lblY_1;
	private JLabel lblZ_1;
	private JButton btnRotate;
	private JLabel label_1;
	private JLabel lblY_2;
	private JLabel lblZ_2;
	private JTextField txtXScale;
	private JTextField txtYScale;
	private JTextField txtZScale;
	private JButton btnScale;
	private JList lstPossibleModels;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private JList lstLoadedModels;
	private JButton btnLoadModelList;
	private JButton btnSaveModelList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//LevelWindow window = new LevelWindow();
					//window.getFrmLevelEditer().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public LevelWindow(InputManager inputManager) {
		initialize();
		this.inputManager = inputManager;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		setFrmLevelEditer(new JFrame());
		getFrmLevelEditer().setTitle("Level Editer");
		getFrmLevelEditer().setBounds(100, 100, 586, 447);
		getFrmLevelEditer().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 413, 134);
		panel.setLayout(new MigLayout("", "[][][][][][grow][][grow][][grow]", "[][grow][][][][][][][][]"));

		JLabel lblX = new JLabel("X:");
		panel.add(lblX, "cell 0 1");

		txtXTrans = new JTextField();
		txtXTrans.setText("0");
		panel.add(txtXTrans, "cell 1 1,growx");
		txtXTrans.setColumns(10);

		label = new JLabel("X:");
		panel.add(label, "cell 2 1");

		txtXRot = new JTextField();
		txtXRot.setText("0");
		txtXRot.setColumns(10);
		panel.add(txtXRot, "cell 3 1,growx");

		label_1 = new JLabel("X:");
		panel.add(label_1, "cell 4 1,alignx trailing");

		txtXScale = new JTextField();
		txtXScale.setText("1");
		txtXScale.setColumns(10);
		panel.add(txtXScale, "cell 5 1,growx");

		JLabel lblY = new JLabel("Y:");
		panel.add(lblY, "cell 0 2,alignx left");

		txtYTrans = new JTextField();
		txtYTrans.setText("0");
		txtYTrans.setColumns(10);
		panel.add(txtYTrans, "cell 1 2,growx");

		txtYRot = new JTextField();
		txtYRot.setText("0");
		txtYRot.setColumns(10);
		panel.add(txtYRot, "cell 3 2,growx");

		lblY_1 = new JLabel("Y:");
		panel.add(lblY_1, "cell 2 2,alignx trailing");

		txtYScale = new JTextField();
		txtYScale.setText("1");
		txtYScale.setColumns(10);
		panel.add(txtYScale, "cell 5 2,growx");

		lblZ = new JLabel("Z:");
		panel.add(lblZ, "cell 0 3,alignx left");

		txtZTrans = new JTextField();
		txtZTrans.setText("0");
		panel.add(txtZTrans, "cell 1 3,growx");
		txtZTrans.setColumns(10);

		txtZRot = new JTextField();
		txtZRot.setText("0");
		txtZRot.setColumns(10);
		panel.add(txtZRot, "cell 3 3,growx");

		txtZScale = new JTextField();
		txtZScale.setText("1");
		txtZScale.setColumns(10);
		panel.add(txtZScale, "cell 5 3,growx");

		btnTranslate = new JButton("Translate");
		btnTranslate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final float x = Float.parseFloat(txtXTrans.getText());
				final float y = Float.parseFloat(txtYTrans.getText());
				final float z = Float.parseFloat(txtZTrans.getText());
				inputManager.translate(x, y, z);
			}
		});
		panel.add(btnTranslate, "cell 1 4,growx");

		lblZ_1 = new JLabel("Z:");
		panel.add(lblZ_1, "cell 2 3");

		btnRotate = new JButton("Rotate");
		btnRotate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final float x = Float.parseFloat(txtXRot.getText());
				final float y = Float.parseFloat(txtYRot.getText());
				final float z = Float.parseFloat(txtZRot.getText());
				inputManager.rotate(x, y, z);
			}
		});
		panel.add(btnRotate, "cell 3 4,growx");

		lblY_2 = new JLabel("Y:");
		panel.add(lblY_2, "cell 4 2,alignx trailing");

		lblZ_2 = new JLabel("Z:");
		panel.add(lblZ_2, "cell 4 3,alignx trailing");

		btnScale = new JButton("Scale");
		btnScale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final float x = Float.parseFloat(txtXScale.getText());
				final float y = Float.parseFloat(txtYScale.getText());
				final float z = Float.parseFloat(txtZScale.getText());
				inputManager.scale(x, y, z);
			}
		});
		panel.add(btnScale, "cell 5 4,growx");
		getFrmLevelEditer().getContentPane().setLayout(null);
		getFrmLevelEditer().getContentPane().add(panel);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 133, 413, 265);
		getFrmLevelEditer().getContentPane().add(panel_1);
		panel_1.setLayout(new MigLayout("", "[23px,grow][89px,grow][][][][]", "[23px,grow][][][][]"));

		JButton btnAddModel = new JButton("Add Model");
		btnAddModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String model = (String) lstPossibleModels.getSelectedValue(); 
				inputManager.spawnModel(modelDir + File.separator + model);
				models.add(model);
				lstLoadedModels.setListData(models.toArray(new Object[models.size()]));
			}
		});
		loadPossibleModels(modelDir);

		scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "cell 0 0,grow");

		lstLoadedModels = new JList();
		lstLoadedModels.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if(inputManager == null)
					return;
				inputManager.selectModel(lstLoadedModels.getSelectedIndex());
			}
		});
		scrollPane.setViewportView(lstLoadedModels);
		lstLoadedModels.setValueIsAdjusting(true);
		lstLoadedModels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstLoadedModels.setModel(new AbstractListModel() {
			String[] values = new String[] {};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});

		scrollPane_1 = new JScrollPane();
		panel_1.add(scrollPane_1, "cell 1 0,grow");

		lstPossibleModels = new JList();
		scrollPane_1.setViewportView(lstPossibleModels);
		lstPossibleModels.setValueIsAdjusting(true);
		lstPossibleModels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstPossibleModels.setModel(new AbstractListModel() {
			String[] values = possibleModels;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		lstPossibleModels.setSelectedIndex(0);
		lstPossibleModels.setBorder(null);
		//JScrollPane scrollPane2 = new JScrollPane(lstPossibleModels);
		//panel_1.add(scrollPane2, "cell 0 8,alignx left,aligny top");
		panel_1.add(btnAddModel, "cell 0 8,alignx left,aligny top");
		
		btnLoadModelList = new JButton("Load Model List");
		btnLoadModelList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(new File("." + File.separator));
				final int returnValue = fc.showOpenDialog(getFrmLevelEditer());
				if(returnValue == JFileChooser.APPROVE_OPTION){
					File file = fc.getSelectedFile();
					inputManager.loadModelList(loadModels(file));
				}
			}
		});
		btnLoadModelList.setBounds(433, 375, 127, 23);
		frmLevelEditer.getContentPane().add(btnLoadModelList);
		
		btnSaveModelList = new JButton("Save Model List");
		btnSaveModelList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(new File("." + File.separator));
				final int returnValue = fc.showSaveDialog(getFrmLevelEditer());
				if(returnValue == JFileChooser.APPROVE_OPTION){
					final File file = fc.getSelectedFile();
					saveModels(file.getParent(), file.getName(), inputManager.buildModelList(models.toArray(new String[models.size()])));
				}
			}
		});
		btnSaveModelList.setBounds(433, 341, 127, 23);
		frmLevelEditer.getContentPane().add(btnSaveModelList);
	}

	public void loadPossibleModels(String string) {
		// TODO Auto-generated method stub
		possibleModels = (new File(string)).list();
	}

	/**
	 * @return the frmLevelEditer
	 */
	public JFrame getFrmLevelEditer() {
		return frmLevelEditer;
	}

	/**
	 * @param frmLevelEditer the frmLevelEditer to set
	 */
	public void setFrmLevelEditer(JFrame frmLevelEditer) {
		this.frmLevelEditer = frmLevelEditer;
	}
	public JList getLstPossibleModels() {
		return lstPossibleModels;
	}
	public JList getLstLoadedModels() {
		return lstLoadedModels;
	}

	public ModelList loadModels(File modelFile){
		Object list = null;
		try {
			FileInputStream fis = new FileInputStream(modelFile); 
			ObjectInputStream ois = new ObjectInputStream(fis);
			list = ois.readObject();
			ois.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (ModelList) list;

	}

	public void saveModels(String fileFolder, String fileName, ModelList modelList){
		try {
			FileOutputStream fos = new FileOutputStream(fileFolder + File.separator + fileName); 
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(modelList);
			oos.flush();
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public JButton getBtnSaveModelList() {
		return btnSaveModelList;
	}
}
