/**
 * 
 */
package me.tempus.levelediter;

import org.lwjgl.util.vector.Vector3f;

import me.tempus.jobs.GLJob;
import me.tempus.jobs.GLJob_ModelSetUp;
import me.tempus.jobs.GLJobi;
import me.tempus.modelutil.ADSModelLoader;
import me.tempus.modelutil.ImpModel;
import me.tempus.modelutil.ModelLoader;
import me.tempus.modelutil.ModelLoaderTemplate;
import me.tempus.util.Utilities;

/**
 * @author Chris
 * Class that manages all input for the level editer
 * Includes input from the GUI window and direct input into the game
 */
public class InputManager implements GLJobi{	

	private GameThread game;
	private Render render;
	private ImpModel activeModel;

	/**
	 * @param game
	 * @param render
	 */
	public InputManager(GameThread game, Render render) {
		super();
		this.game = game;
		this.render = render;
	}

	public void spawnModel(String modelName){
		Render.addJob(new GLJob_ModelSetUp(modelName, this));
	}

	public void translate(float x, float y, float z){
		if(activeModel != null){
			final Vector3f translate = new Vector3f(x,y,z);
			activeModel.setTranslate(translate);
		}
	}

	public void scale(float x, float y, float z){
		if(activeModel != null){
			final Vector3f scale = new Vector3f(x, y, z);
			activeModel.setScale(scale);
		}
	}

	public void rotate(float x, float y, float z){
		if(activeModel != null){
			final Vector3f rotate = new Vector3f(x, y, z);
			activeModel.setRotate(rotate);
		}
	}
	
	/**
	 * Make sure the model names posistion is identical to the one in the gamethread's
	 * @param modelNames
	 * @return
	 */
	public ModelList buildModelList(String[] modelNames){
		final int size = modelNames.length;
		final Transformation[] transformations = new Transformation[size];
		for(int i = 0; i < size; i++){
			final ImpModel currentModel = game.getModel(i);
			transformations[i] = new Transformation(currentModel.getTranslate(), currentModel.getRotate(), currentModel.getScale());
		}
		return new ModelList(modelNames, transformations);
	}
	
	public void loadModelList(ModelList list){
		final String[] modelNames = list.getModelNames();
		final Transformation[] transformations = list.getTransformations();
		final int size = modelNames.length;
		for(int i = 0; i < size; i++){
			final Transformation currentTransformation = transformations[i];
			Render.addJob(new GLJob_ModelSetUp(modelNames[i], this, currentTransformation.getTranslate(), currentTransformation.getScale(), currentTransformation.getRotate()));
		}
	}

	/**
	 * 
	 * @param index The index in which the model is in the game thread array of models
	 */
	public void selectModel(int index){
		activeModel = game.getModel(index);
	}

	public void removeModel(){

	}

	public void keyPressed(int state, int key){

	}

	@Override
	public void postResults(GLJob jobInstance, Object... objects) {
		// TODO Auto-generated method stub
		game.addModel((ImpModel) objects[0]);
	}
}
