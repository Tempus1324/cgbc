/**
 * 
 */
package me.tempus.levelediter;

import java.awt.EventQueue;
import java.io.File;

import me.tempus.jobs.GLJob;
import me.tempus.jobs.GLJob_ShaderSetUp;
import me.tempus.jobs.GLJobi;
import me.tempus.shader.ADSShader;
import me.tempus.shader.ADSTAShader;
import me.tempus.shader.Shader;
import me.tempus.util.PVM;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.vector.Vector3f;

/**
 * @author Chris
 *
 */
public class LevelEditer {

	public static GameThread game;
	public static Render renderGame;
	public static InputManager input;
	private static LevelWindow levelGUI;
	private static boolean done = false;
	private static boolean wait = false;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new LevelEditer().run();
	}

	public void run(){
		setUpGame();
		init();		
		spawnGUI();
		wait = true;
		while(!done){
			if(wait){
				try {
					synchronized (this) {
						this.wait();
					}					
					wait = false;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			update();
		}
		cleanUp();
	}

	private void setUpGame() {
		renderGame = new Render(this);
		game = new GameThread(renderGame);
		(new Thread(game)).start();
		(new Thread(renderGame)).start();
	}

	public void update(){
		while(Keyboard.next()){
			if(Keyboard.getEventKeyState()){
				switch(Keyboard.getEventKey()){
				case Keyboard.KEY_ESCAPE:
					game.setDone();
					renderGame.setDone();
					this.done = true;
					break;
				}
				input.keyPressed(1, Keyboard.getEventKey());
			}
		}
	}	

	public void init(){
		input = new InputManager(game, renderGame);
	}

	public void cleanUp(){

	}

	public void renderThreadSetUp(){
		synchronized (game) {
			game.notify();
		}
		synchronized (this) {
			this.notify();
		}		
	}

	public void spawnGUI(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					levelGUI = new LevelWindow(input);
					levelGUI.getFrmLevelEditer().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
