/**
 * 
 */
package me.tempus.levelediter;

import java.io.File;
import java.util.ArrayList;

import me.tempus.jobs.GLJob;
import me.tempus.jobs.GLJob_ShaderSetUp;
import me.tempus.modelutil.ImpModel;
import me.tempus.shader.ADSShader;
import me.tempus.shader.Shader;
import me.tempus.util.Camera3;
import me.tempus.util.PVM;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.Drawable;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.vector.Vector3f;

/**
 * @author Chris
 *
 */
public class Render implements Runnable {

	private boolean fullscreen = false;
	private String TITLE = "Level Editor";
	private static final int width = 640;
	private static final int height = 480;

	public static Drawable drawable;

	private final static ArrayList<ImpModel> models= new ArrayList<ImpModel>();
	private final static ArrayList<GLJob> jobs = new ArrayList<GLJob>();
	private static boolean done = false;
	private static boolean wait = false;

	private LevelEditer levelEditerInstance;
	
	public static Shader[] shaders = new Shader[1];

	public Render(LevelEditer instance){
		levelEditerInstance = instance;
	}

	public void init(){
		try {
			//Display.setDisplayMode(Display.getDisplayMode());
			//Display.getDrawable().makeCurrent();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void draw(){
		GL20.glUseProgram(shaders[0].getShaderID());
		PVM.loadIdentity();
		Camera3.transform();
		int size = models.size();
		//System.out.println("Render models size: " + size);
		for(int i = 0; i < size; i++){
			models.get(i).draw();
		}
		models.clear();
		models.trimToSize();
		GL20.glUseProgram(0);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		init();
		setUpGameWindow();
		setUpShaders();
		levelEditerInstance.renderThreadSetUp();
		while(!done){
			//if(wait)
			callWait();
			final long currentTime = System.currentTimeMillis();
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);


			GLJob[] job = jobs.toArray(new GLJob[jobs.size()]);
			jobs.clear();
			jobs.trimToSize();
			for(GLJob currentJob : job){
				currentJob.executeTask();
			}
			draw();
			Display.update();
			Display.sync(60);
			final long delta = System.currentTimeMillis() - currentTime;
		}
		cleanUp();
	}

	public void addRenderable(ArrayList<ImpModel> models){
		models.trimToSize();
		this.models.addAll(models);
	}

	public void beginFrame(){
		synchronized (this) {
			this.notify();
		}
	}

	public void setDone(){
		done = true;
	}

	public void cleanUp(){

	}

	public void addToDraw(ImpModel model){		
		models.add(model);
	}

	public static void addJob(GLJob job){
		jobs.add(job);
	}

	public void removeJob(GLJob job){
		jobs.remove(job);
	}

	private void callWait(){
		try {
			synchronized (this) {
				this.wait();
			}
			wait = false;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void setUpShaders(){
		Shader shader;
		//shader = new ADSTAShader("Shaders" + File.separatorChar + "ADSTA.vert", "Shaders" + File.separatorChar + "ADSTA.frag");
		//Render.addJob(new GLJob_ShaderSetUp(shader));
		shader = new ADSShader("Shaders" + File.separatorChar + "ADS.vert", "Shaders" + File.separatorChar + "ADS.frag");
		shader.loadShaders();
		((ADSShader)shader).setLightPosistion(new Vector3f(0,0,-100));
		shaders[0] = (ADSShader) shader;
	}
	
	public void setUpGameWindow(){
		try {
			PixelFormat pixelFormat = new PixelFormat();
			ContextAttribs contextAtrributes = new ContextAttribs(3, 2);
			contextAtrributes.withForwardCompatible(true);
			contextAtrributes.withProfileCore(true);

			Display.setFullscreen(fullscreen);

			Display.setDisplayMode(new DisplayMode(width, height));
			Display.setTitle(TITLE);
			//Display.create(pixelFormat, contextAtrributes);
			Display.create();
			GL11.glViewport(0, 0, width, height);

			GL11.glEnable(GL11.GL_TEXTURE_2D);                          // Enable Texture Mapping
			GL11.glShadeModel(GL11.GL_SMOOTH);                          // Enables Smooth Color Shading
			GL11.glClearColor(0.5f, 0.5f, 0.5f, 0.0f);                // This Will Clear The Background Color To Black
			GL11.glClearDepth(1.0);                                   // Enables Clearing Of The Depth Buffer
			GL11.glEnable(GL11.GL_DEPTH_TEST);                          // Enables Depth Testing
			GL11.glDepthFunc(GL11.GL_LEQUAL);                           // The Type Of Depth Test To Do
			PVM.setUpProjection(45f, width, height, 0.1f, 100f);
			System.out.println("OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));
			String extension = "GL_EXT_texture_array";
			//if((GL11.glGetString(GL11.GL_EXTENSIONS)).contains(extension)){
			//System.out.println(extension + " is an extension");
			//}

			//drawable = Display.getDrawable();
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
