/**
 * 
 */
package me.tempus.levelediter;

import java.util.ArrayList;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.Drawable;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.SharedDrawable;

import me.tempus.modelutil.ImpModel;

/**
 * @author Chris
 *
 */
public class GameThread implements Runnable {

	private static boolean done = false;
	private static boolean wait = false;

	public static final ArrayList<ImpModel> models = new ArrayList<ImpModel>();
	private Render renderGame;

	public GameThread(Render renderGame) {
		this.renderGame = renderGame;
	}

	/** (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		callWait(-1);
		init();
		while(!done){
			if(wait){
				callWait(-1);
			}
			final long currentTime = System.currentTimeMillis();
			
			
			draw();

			renderGame.beginFrame();
			final long delta = System.currentTimeMillis() - currentTime;
			if(delta < 16){
				synchronized (this) {
					try {
						this.wait(16 - delta);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
		cleanUp();

	}

	/**
	 * The logic setup for the game
	 */
	public void init(){
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(){

	}

	/**
	 * Push any and all data to render thread
	 */
	public void draw(){
		renderGame.addRenderable(models);
	}

	public void addModel(ImpModel model){
		models.add(model);
	}
	
	public void removeModel(int index){
		models.remove(index);
	}
	
	public ImpModel getModel(int index){
		return models.get(index);
	}

	private void cleanUp() {

	}

	private void callWait(int milli){
		try {
			synchronized (this) {
				if(milli <= 0){
					this.wait();	
				}else{
					this.wait(milli);
				}
			}
			wait = false;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setDone(){
		done = true;
	}

	public void setWait(boolean value){
		this.wait = value;
	}





}
