package me.tempus.levelediter;

import java.io.Serializable;

import org.lwjgl.util.vector.Vector3f;

/**
 * 
 * @author Chris
 *
 */
public class ModelList implements Serializable{

	String[] modelNames;
	Transformation[] transformations;
	/**
	 * @param modelNames
	 * @param transformations
	 */
	public ModelList(String[] modelNames, Transformation[] transformations) {
		super();
		this.modelNames = modelNames;
		this.transformations = transformations;
	}
	/**
	 * @return the modelNames
	 */
	public String[] getModelNames() {
		return modelNames;
	}
	/**
	 * @return the transformations
	 */
	public Transformation[] getTransformations() {
		return transformations;
	}
	
	
	
}

class Transformation implements Serializable{
	private Vector3f translate;
	private Vector3f rotate;
	private Vector3f scale;
	/**
	 * @param translate
	 * @param rotate
	 * @param scale
	 */
	public Transformation(Vector3f translate, Vector3f rotate, Vector3f scale) {
		super();
		this.translate = translate;
		this.rotate = rotate;
		this.scale = scale;
	}
	/**
	 * @return the translate
	 */
	public Vector3f getTranslate() {
		return translate;
	}
	/**
	 * @return the rotate
	 */
	public Vector3f getRotate() {
		return rotate;
	}
	/**
	 * @return the scale
	 */
	public Vector3f getScale() {
		return scale;
	}
	
	
}
