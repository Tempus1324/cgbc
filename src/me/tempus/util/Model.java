package me.tempus.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.tempus.modelutil.Material;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

public class Model implements Serializable {

	public static final long serialVersionUID = 100L;

	private List<Vector3f> vertices = new ArrayList<Vector3f>();
	private List<Vector3f> normals = new ArrayList<Vector3f>();

	private List<Face> faces = new ArrayList<Face>();

	private HashMap<String, Material> materials = new HashMap();

	private transient int displayList;

	public Model(){

	}

	public void addMaterial(String name, Material material){
		materials.put(name, material);
	}

	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException{
		in.defaultReadObject();
		/*
		System.out.println("Model loaded");
		System.out.println(vertices.size() + " Vertices loaded");
		System.out.println(normals.size() + " Normals loaded");
		System.out.println(faces.size() + " Faces loaded");
		*/
		//genDisplayList();
	}

/*
	public void genDisplayList(){
		int displayList = glGenLists(1);
		String lastMaterial = "";

		glNewList(displayList, GL_COMPILE);
		{
			//glColor3f(1f, 0f, 0f);
			//glMaterialf(GL_FRONT, GL_SHININESS, 128.0f);
			
			glBegin(GL_TRIANGLES);
			for (Face face : faces) {
				try{

					if(!(face.getMaterialName().equalsIgnoreCase(lastMaterial))){
						lastMaterial = face.getMaterialName();
						Material mat = materials.get(face.getMaterialName());
						//glMaterialf(GL_FRONT, GL_SHININESS, mat.getShine());
						
												
						
						//glMaterial(GL_FRONT, GL_AMBIENT, (Utilities.asFloatBuffer4(mat.getAmbient())));
						//glMaterial(GL_FRONT, GL_DIFFUSE, (Utilities.asFloatBuffer4(mat.getDiffuse())));
						//glMaterial(GL_FRONT, GL_SPECULAR, (Utilities.asFloatBuffer4(mat.getSpectual())));
						
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				Vector3f n1 = normals.get((int) face.getNormal().x - 1);
				glNormal3f(n1.x, n1.y, n1.z);
				Vector3f v1 = vertices.get((int) face.getVertex().x - 1);
				glVertex3f(v1.x, v1.y, v1.z);
				Vector3f n2 = normals.get((int) face.getNormal().y - 1);
				glNormal3f(n2.x, n2.y, n2.z);
				Vector3f v2 = vertices.get((int) face.getVertex().y - 1);
				glVertex3f(v2.x, v2.y, v2.z);
				Vector3f n3 = normals.get((int) face.getNormal().z - 1);
				glNormal3f(n3.x, n3.y, n3.z);
				Vector3f v3 = vertices.get((int) face.getVertex().z - 1);
				glVertex3f(v3.x, v3.y, v3.z);
			}
			glEnd();
		}
		glEndList();
		
		if(GL11.glGetError() != GL_NO_ERROR){
			System.out.println("Model L95 Error: " + GL11.glGetError());
		}
		this.displayList = displayList;

	}
*/


	protected void addVertext(Vector3f vector){
		vertices.add(vector);
	}

	protected void addNormal(Vector3f vector){
		normals.add(vector);
	}

	protected void addFace(Face face){
		faces.add(face);
	}

	/**
	 * @return the vertices
	 */
	protected List<Vector3f> getVertices() {
		return vertices;
	}

	/**
	 * @param vertices the vertices to set
	 */
	protected void setVertices(List<Vector3f> vertices) {
		this.vertices = vertices;
	}

	/**
	 * @return the normals
	 */
	protected List<Vector3f> getNormals() {
		return normals;

	}

	/**
	 * @param normals the normals to set
	 */
	protected void setNormals(List<Vector3f> normals) {
		this.normals = normals;
	}

	/**
	 * @return the faces
	 */
	protected List<Face> getFaces() {
		return faces;
	}

	/**
	 * @param faces the faces to set
	 */
	protected void setFaces(List<Face> faces) {
		this.faces = faces;
	}

	public int getDisplayList(){
		return displayList;
	}



}
