/**
 * 
 */
package me.tempus.util;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.lwjgl.BufferUtils;

/**
 * @author Chris
 *
 */
public class Text {
	
	/**
	 * Possibly use draw arrays to avoid idincies
	 * Need to re-create the methods to create the VAO without indincies
	 * Be easier to have a hashmap with the (key:) text and the it's (value:) location I need to render?
	 * 	Allow for more efficient processing? Passes through a filter before adding to hashmap to see how much I need in my float arrays
	 * 		Adds to int value for the length of the string, each character
	 * Easier to use a pre-built hashmap with all the characters and their indicines
	 * 	Indinices are the coords in the float buffers
	 */
	
	private HashMap<Integer, Character> characters = new HashMap();
	private String imageFile;
	
	private List<Piece> characterList = new ArrayList<Piece>();
	
	FloatBuffer vertices;
	FloatBuffer textureCoords;
	private int verticesSize;

	public Text(){
		
		
	}
	
	/**
	 * TODO Add texture coords for each letter
	 */
	
	
	public void update(){
		vertices = BufferUtils.createFloatBuffer(characterList.size() *12);
		for(Piece letter : characterList){
			Character letterCharacter = characters.get(letter.getAsciiCode());
			vertices.put(makeTriangle(letter.getX(), letter.getY(), letterCharacter.getWidth(), letterCharacter.getHeight()));
		}
	}
	
	public void drawString(String string, float x, float y){
		/**
		 * TODO Create methods here that generators a square with the character from the texture atlas using the asciicode
		 * Need the asciicode * the width of each chacter for the x posistion on the texture atlas
		 * 
		 * Possibly ignore below
		 * Going to always boil down to looping through an array and pushing the vertices based on the characters.
		 * TODO Replace code below with code that loops the string and buffers each character to be rendered 
		 **/
		char[] chars = string.toCharArray();
		int xAdvance = 0;
		for(int i = 0; i < string.length(); i++){
			Character chara = characters.get((int)(string.charAt(i)));
			characterList.add(new Piece(chara.getID(), x + xAdvance, y));
			
			xAdvance = chara.getxAdvance();
		}
		
	}
	
	public float[] makeTriangle(float x, float y, float width, float height){
		
		return new float[]{
				x, y + height, 0,
				x, y, 0,
				x + width, y + height, 0,
				x + width, y, 0};
	}
	
	
	
	
}

class Piece{
	private int asciiCode;
	private float x;
	private float y;
	/**
	 * @param asciiCode
	 * @param x
	 * @param y
	 */
	public Piece(int asciiCode, float x, float y) {
		super();
		this.asciiCode = asciiCode;
		this.x = x;
		this.y = y;
	}
	/**
	 * @return the asciiCode
	 */
	public int getAsciiCode() {
		return asciiCode;
	}
	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}
	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}


	
	
	
	
}
