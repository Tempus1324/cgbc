package me.tempus.util;

public class Time {
	
	private static long lastFrame;

	/**
	 * 
	 * @return Time since last frame in miliseconds
	 */
	
	public static int getDelta() {
	    long time = getTime();
	    int delta = (int) (time - lastFrame);
	    lastFrame = time;
	    	
	    return delta;
	}
	
	/**
	 * Get the time in milliseconds
	 * 
	 * @return The system time in milliseconds
	 */
	public static long getTime() {
	    return System.nanoTime() / 1000000;
	}
	
	static {
		lastFrame = getTime();
	}
}
