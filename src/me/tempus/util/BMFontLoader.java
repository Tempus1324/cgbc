/**
 * 
 */
package me.tempus.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author Chris
 *
 */
public class BMFontLoader {

	private HashMap<Integer, Character> characters = new HashMap();
	private String imageFile;

	public BMFontLoader(){

	}

	public void load(String folderPath, String fileName) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(folderPath + File.separatorChar + fileName));
		String line;
		while((line = reader.readLine()) != null){
			if(line.startsWith("char ")){
				readLine(line);
			}
			
		}
	}
	
	public void readLine(String line){
		String[] values = line.split(" ");

		//Values spliting		
		int ID = Integer.parseInt((values[1].split("="))[1]);
		int x = Integer.parseInt((values[2].split("="))[1]);
		int y = Integer.parseInt((values[3].split("="))[1]);
		int width = Integer.parseInt((values[4].split("="))[1]);
		int height = Integer.parseInt((values[5].split("="))[1]);
		int xAdvance = Integer.parseInt((values[8].split("="))[1]);
		createCharacter(ID, x, y, width, height, xAdvance);
	}

	public void createCharacter(int ID, int x, int y, int width, int height, int xAdvance){
		characters.put(ID, new Character(ID, x, y, width, height, xAdvance));
	}

	public static void main(String[] args){
		System.out.println("Please enter the name of the model file to be loaded (*.obj): ");
		Scanner scanner = new Scanner(System.in);
		String[] values = scanner.nextLine().split(" ");
		String path = values[1];
		String folder = values[0];

		scanner.close();
		try{
			System.out.println("Models" + File.separator + path);
			FileOutputStream fos = new FileOutputStream("Models" + File.separator + path); 
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject("hi");
			oos.flush();
			oos.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

}

class Character{

	private int ID;
	private int x;
	private int y;
	private int width;
	private int height;
	private int xAdvance;
	/**
	 * @param iD
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Character(int iD, int x, int y, int width, int height, int xAdvance) {
		ID = iD;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.xAdvance = xAdvance;
	}
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}
	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}
	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}
	/**
	 * @return the xAdvance
	 */
	public int getxAdvance() {
		return xAdvance;
	}

	

}
