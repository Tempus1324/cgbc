package me.tempus.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.tempus.modelutil.Material;

public class Face implements Serializable{
	public static final long serialVersionUID = 100L;
	
	private HashMap<String, Integer> loadedFaces = new HashMap<String, Integer>();
	private List<String> faces = new ArrayList<String>();
	private int faceIndex = 0;
	private Material material;
	
	public Face(Material material){
		this.material = material;
	}
	
	public void loadFace(String face){
		faces.add(face);
		if(!(loadedFaces.containsKey(face))){
			loadedFaces.put(face, faceIndex);			
			faceIndex++;
		}
	}

	/**
	 * @return the loadedFaces
	 */
	public HashMap<String, Integer> getLoadedFaces() {
		return loadedFaces;
	}

	/**
	 * @return the faces
	 */
	public List<String> getFaces() {
		return faces;
	}

	/**
	 * @return the material
	 */
	public Material getMaterial() {
		System.out.println(material);
		return material;
	}
	
	
	
	
}
