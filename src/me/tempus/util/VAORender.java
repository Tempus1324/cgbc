/**
 * 
 */
package me.tempus.util;

import static org.lwjgl.opengl.GL11.GL_FLOAT;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/**
 * @author Chris
 * TODO Render with draw elements and indicies
 *
 */
public class VAORender {

	/**
	 * 
	 * @param vao the ID of the VAO to be rendered
	 * @param attributes A array of the attributes to be enabled
	 * @param count The amount of triangles to drawn. Generally vertices length / 3
	 */

	public static void render(int vao, int indicesID, int[] attributes, int count){
		// Bind to the VAO that has all the information about the quad vertices
		GL30.glBindVertexArray(vao);
		for(int i = 0; i < attributes.length; i++){
			GL20.glEnableVertexAttribArray(attributes[i]);
		}
		// Draw the vertices
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesID);
		//GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, count);
		GL11.glDrawElements(GL11.GL_TRIANGLES, count, GL11.GL_UNSIGNED_INT, 0);

		// Put everything back to default (deselect)
		for(int i = 0; i < attributes.length; i++){
			GL20.glDisableVertexAttribArray(attributes[i]);
		}
		GL30.glBindVertexArray(0);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	/**
	 * 
	 * @param floats The arrays of data to in the VAO. Eg: Vertices, Colours, Texture Coordinates
	 * @param sizes The respective sizes of the vertices in each of the arrays in the list
	 * @return vao to bind when drawing
	 */

	public static int[] setUp(List<FloatBuffer> floats, int[] sizes, IntBuffer indicesBuffer){
		// TODO Optimize this code and any references to it

		int vao = -1;
		int indicesID = -1;
		vao = GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vao);
		/*
		int size = 0;
		for(int i = 0; i < floats.size(); i++){
			size += (floats.get(i)).length;
		}

		FloatBuffer buffer = BufferUtils.createFloatBuffer(size);

		for(int i = 0; i < floats.size(); i++){
			//float[] current = ImpModel.tofloatArray(floats.get(i));
			buffer.put(ImpModel.tofloatArray(floats.get(i))); // Cyles through arrays and adds it to the buffer
		}
		buffer.flip();

		int vbo = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);

		int floatByteSize = 4;

		//int floatsPerVertex = 0;
		for(int i: sizes){
			//floatsPerVertex += i;
		}
		int byteOffSet = 0;
		int stride = 0;
		for(int i = 0; i < floats.size(); i++){			

			if(i > 0){				
				byteOffSet += (floatByteSize * sizes[i -1]);				
			}

			stride = floatByteSize * sizes[i];
			System.out.println("Stride: " + byteOffSet);
			GL20.glVertexAttribPointer(i, sizes[i], GL11.GL_FLOAT, false, stride, byteOffSet);
		}
		 */
		int floatByteSize = 4;
		int vbo1 = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo1);
		//FloatBuffer buffer = BufferUtils.createFloatBuffer((floats.get(0)).length);
		//buffer.put(floats.get(0));
		//buffer.flip();
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, floats.get(0), GL15.GL_STATIC_DRAW); 



		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 12, 0);

		if(floats.size() >= 2){
			int vbo2 = GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo2);
			//buffer = BufferUtils.createFloatBuffer((floats.get(1)).length);
			//buffer.put(floats.get(1));
			//buffer.flip();
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, floats.get(1), GL15.GL_STATIC_DRAW);

			GL20.glVertexAttribPointer(1, sizes[1], GL11.GL_FLOAT, false, 0, 0);
		}

		if(floats.size() >= 3){
			int vbo3 = GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo3);
			//buffer = BufferUtils.createFloatBuffer((floats.get(2)).length);
			//buffer.put(floats.get(2));
			//buffer.flip();
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, floats.get(2), GL15.GL_STATIC_DRAW);

			GL20.glVertexAttribPointer(2, sizes[2], GL11.GL_FLOAT, false, 0, 0);
		}
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL30.glBindVertexArray(0);

		indicesID = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesID);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);

		return new int[]{vao, indicesID};
	}

}
