package me.tempus.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Utilities {

	public static FloatBuffer asFloatBuffer(float[] values){
		FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
		buffer.put(values);
		//Create a directly allocated buffer in bytes (16 = 4 Floats * size of float in bytes)
		//ByteBuffer bytes = ByteBuffer.allocateDirect(values.length * 4).order(ByteOrder.nativeOrder());
		//FloatBuffer buffer = bytes.asFloatBuffer();
		
		//buffer.put(values);
		//buffer.rewind();
		buffer.flip();
		return buffer;
	}
	
	public static FloatBuffer asFloatBuffer4(float[] values){
		ByteBuffer bytes = ByteBuffer.allocateDirect(4 * 4).order(ByteOrder.nativeOrder());
		FloatBuffer buffer = bytes.asFloatBuffer();
		
		buffer.put(values);
		buffer.rewind();
		return buffer;
	}
	
	public static Texture getTexture(String key, String file){
		try{
			return TextureLoader.getTexture(key,
					ResourceLoader.getResourceAsStream(file + "." + key.toLowerCase()));
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static int loadTextureArray(String folder, String[] files, int textureHeight, int textureWidth, String key){
		int ID = -1;
		
		ID = GL11.glGenTextures();
		GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, ID);
		
		GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE); 
		GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE); 
		GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
		GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		
		ByteBuffer j = null;
		GL12.glTexImage3D(GL30.GL_TEXTURE_2D_ARRAY, 0, GL11.GL_RGBA, textureWidth, textureHeight, files.length, 0, 
				GL12.GL_BGRA, GL11.GL_UNSIGNED_BYTE, j);
		
		for(int i = 0; i < files.length; i++){
			Texture texture = getTexture(key, folder + File.separatorChar + files[i]);
			byte[] b = texture.getTextureData();
			ByteBuffer bytes = BufferUtils.createByteBuffer(b.length);
			bytes.put(b);
			bytes.flip();
			GL12.glTexSubImage3D(GL30.GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, textureWidth, textureHeight, 1, GL12.GL_BGRA, GL11.GL_UNSIGNED_BYTE, bytes);
		}
		
		/**
		 * for(int i = 0; i < 29; i++) { 
			char cFile[32]; 
			sprintf(cFile, �moon%02d.tga�, i);
			GLbyte *pBits; 
			int nWidth, nHeight, nComponents; 
			GLenum eFormat;
			// Read the texture bits 
			pBits = gltReadTGABits(cFile, &nWidth, &nHeight, &nComponents, &eFormat); 
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, nWidth, nHeight, 
			1, GL_BGRA, GL_UNSIGNED_BYTE, pBits);
			free(pBits); 
			}
		 */
		GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, 0);
		return ID;
	}
	
	/**
	 * 
	 * @param vertPath Path to vertex (vert) shader source
	 * @param fragPath Path to fragment (frag) shader source
	 * @return Vertsouce and FragSouce in stringbuilder array. VertSource at posistion 0 in array
	 */
	
	public static StringBuilder[] loadShaders(String vertPath, String fragPath){
		
		StringBuilder vertSource = new StringBuilder();
		StringBuilder fragSource = new StringBuilder();
		
		try{
			BufferedReader reader = new BufferedReader(new FileReader(vertPath));
			String line;
			while((line = reader.readLine()) != null){
				vertSource.append(line).append("\n");
			}
			reader.close();
			BufferedReader reader1 = new BufferedReader(new FileReader(fragPath));
			while((line = reader1.readLine()) != null){
				fragSource.append(line).append("\n");
			}
			reader1.close();
		}catch(Exception e){
			e.printStackTrace();
		}
				
		
		return new StringBuilder[]{vertSource, fragSource};
	}
	
}
