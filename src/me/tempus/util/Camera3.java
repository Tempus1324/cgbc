/**
 * 
 */
package me.tempus.util;

import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

/**
 * @author Chris
 *
 */
public class Camera3 {

	private static float x;
	private static float y;
	private static float z;
	private static float zrot = 0f; //Roll
	private static float xrot = 0f; //Pitch
	private static float yrot = 0f; //Yall
	private static Vector3f scale;
	
	private static float speed;
	
	public final static float TORADIANS = 0.17f;
	private final static float TODEGREES = 57.296f;
	
	public static void processAngles(){
		
		if(!Mouse.isButtonDown(0)){
			return;
		}
        float mouseDX = Mouse.getDX() * speed * TORADIANS;
        float mouseDY = Mouse.getDY() * speed * TORADIANS;
        
        if (yrot + mouseDX >= 360) {
            yrot = yrot + mouseDX - 360;
        } else if (yrot + mouseDX < 0) {
            yrot = 360 - yrot + mouseDX;
        } else {
            yrot += mouseDX;
        }
        if (xrot - mouseDY < -91f) {
            xrot = -90f;
        } else if (xrot - mouseDY > 91f) {
            xrot = 90f;
        } else{
        	xrot += mouseDY;
        }
        
        
	}
	
	public static void setSpeed(float speed){
		Camera3.speed = speed;
	}
	
	public static void transform(){
        PVM.translate(-x, -y, -z);
        PVM.rotate(-xrot, -yrot, -zrot);
	}
	
	public static void addX(float x){
		Camera3.x += x;
	}
	
	public static void addY(float y){
		Camera3.y += y;
	}
	
	public static void addZ(float z){
		Camera3.z += z;
	}
	
	public static void rotX(float xrot){
		Camera3.xrot += xrot;
	}
	
	public static void rotY(float yrot){
		Camera3.yrot += yrot;
	}
	
	public static void rotZ(float zrot){
		Camera3.zrot += zrot;
	}
	
	public static void moveUp(int upOrDown){
		//Camera3.addY(upOrDown * ((float) (0.01f * z * (sin((toRadians(xrot)))))));
	}
	
	public static Vector3f getCameraPos(){
		return new Vector3f(-x, -y, -z);
	}
	
	public static Vector3f getCameraRot(){
		return new Vector3f(xrot, yrot, zrot);
	}
	
	static{
		x = 0;
		y = 0;
		z = 0;
		scale = new Vector3f(1, 1, 1);
		speed = 0.1f;
	}
}
