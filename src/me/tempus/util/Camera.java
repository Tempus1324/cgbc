/**
 * 
 */
package me.tempus.util;

import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

/**
 * @author Chris
 *
 */
public class Camera {

	private float x;
	private float y;
	private float z;
	private float zrot = 0f; //Roll
	private float xrot = 0f; //Pitch
	private float yrot = 0f; //Yall
	
	public final static float TORADIANS = 0.17f;
	private final static float TODEGREES = 57.296f;
	
	public Camera(float x, float y, float z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Moves the camera by x,y,z and respective rotations
	 */
	
	public void moveCamera(){	
		processAngles(1f);
		
		GL11.glMatrixMode(GL11.GL_MATRIX_MODE);
		GL11.glLoadIdentity();
		GL11.glRotatef(zrot, 0f, 0f, 1f);
		GL11.glRotatef(yrot, 0f, 1f, 0f);
		GL11.glRotatef(-xrot, 1f, 0f, 0f);
		
		GL11.glTranslatef(-x, -y, -z);
		
	}
	
	public void processAngles(float mouseSpeed){
		
		if(!Mouse.isButtonDown(0)){
			return;
		}
        float mouseDX = Mouse.getDX() * mouseSpeed * TORADIANS;
        float mouseDY = Mouse.getDY() * mouseSpeed * TORADIANS;
        
        if (yrot + mouseDX >= 360) {
            yrot = yrot + mouseDX - 360;
        } else if (yrot + mouseDX < 0) {
            yrot = 360 - yrot + mouseDX;
        } else {
            yrot += mouseDX;
        }
        if (xrot - mouseDY < -90f) {
            xrot = -90f;
        } else if (xrot - mouseDY > 90f) {
            xrot = 90f;
        } else{
        	xrot += mouseDY;
        }
        
        //this.y += dy * (float) sin(toRadians(xrot - 90)) + dz * sin(toRadians(xrot));
        //this.y += 0.01f * (sin((xrot - 90 * TORADIANS)) * z);
        
        /**
         *         if (xrot - mouseDY >= -90f
                && xrot - mouseDY <= 90f) {
            xrot += -mouseDY;
        }
         */
	}

	
	
	
	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * @param z the z to set
	 */
	public void setZ(float z) {
		this.z = z;
	}

	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x the x to add
	 */
	public void addX(float x) {
		this.x += x;
	}

	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}

	/**
	 * @param y the y to add
	 */
	public void addY(float dy) {
		this.y += dy;
	}

	/**
	 * @return the z
	 */
	public float getZ() {
		return z;
	}

	/**
	 * @param z the z to add
	 */
	public void addZ(float z) {
		this.z += z;
	}

	/**
	 * @return the zrot
	 */
	public float getZrot() {
		return zrot;
	}

	/**
	 * @param zrot the zrot to set
	 */
	public void setZrot(float zrot) {
		this.zrot += zrot;
	}

	/**
	 * @return the xrot
	 */
	public float getXrot() {
		return xrot;
	}

	/**
	 * @param xrot the xrot to set
	 */
	public void addXrot(float xrot) {
		this.xrot += xrot;
		
	}
	
	public void setXrot(float xrot){
		this.xrot = xrot;
	}

	/**
	 * @return the yrot
	 */
	public float getYrot() {
		return yrot;
	}

	/**
	 * @param yrot the yrot to set
	 */
	public void setYrot(float yrot) {
		this.yrot = yrot;
	}

		
	
}
