package me.tempus.src;


/**
 * Each rendering batch will have it's own shader. Getting ride of shader binding before each rendering call
 * Each batch is definded by it's shader. For instance, each box will be using a specific shader that allows batching of transformations
 * Each class that manages a different batch has the prefix Batch_
 */
/**
 * 
 * @author Chris
 *	Will serve as an over all controller of the game. Starting and stopping logic and render threads
 */

public class Game {
	
	
	
	public static Game instance;

	private static boolean done = false;
	
	public static Logic logic;
	public static Render render;
	
	private static int threadsLoaded = 0;
	private static final int nuThreads = 2;
	
	public static void init(){
		logic = new Logic();
		render = new Render();
	}
	
	public static void setUpThreads(){

		/**
		 * TODO The threads should say when they're completed and wait until the other threads are done
		 */
		(new Thread(logic)).start();
		(new Thread(render)).start();
	}
	
	public static void run(){
	
		init();
		setUpThreads();
		while(!done){
			update();
		}
	}
	
	public static void update(){
		
	}
	
	public static void threadLoaded(){
		threadsLoaded++;
		if(threadsLoaded >= nuThreads){
			// All threads are done
		}
	}
	
	public static void main(String[] args){
		
	}
}
