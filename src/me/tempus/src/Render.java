package me.tempus.src;

import me.tempus.jobs.GLJob;
import me.tempus.jobs.GLJobManager;
import me.tempus.util.PVM;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;


/**
 *
 */


/**
 * @author Chris
 * Base loop that manages the rendering to screen
 */


public class Render implements Runnable {

	private boolean done = false;
	
	private static String extensions = null;

	private GLJobManager jobManager;
	private boolean jobsWaiting = false;
	private boolean startFrame = false;

	public void init(){
		jobManager = new GLJobManager();
	}
	
	public void setUpScreen(boolean fullscreen, int width, int height, String title, boolean contextSpecific){
		try {
			PixelFormat pixelFormat = new PixelFormat();
			ContextAttribs contextAtrributes = new ContextAttribs(3, 2);
			contextAtrributes.withForwardCompatible(true);
			contextAtrributes.withProfileCore(true);

			Display.setFullscreen(fullscreen);

			Display.setDisplayMode(new DisplayMode(width, height));
			Display.setTitle(title);
			if(contextSpecific){
				Display.create(pixelFormat, contextAtrributes);
			}else{
				Display.create();
			}
			
			GL11.glViewport(0, 0, width, height);

			GL11.glEnable(GL11.GL_TEXTURE_2D);                          // Enable Texture Mapping
			GL11.glShadeModel(GL11.GL_SMOOTH);                          // Enables Smooth Color Shading
			GL11.glClearColor(0.5f, 0.5f, 0.5f, 0.0f);                // This Will Clear The Background Color To Black
			GL11.glClearDepth(1.0);                                   // Enables Clearing Of The Depth Buffer
			GL11.glEnable(GL11.GL_DEPTH_TEST);                          // Enables Depth Testing
			GL11.glDepthFunc(GL11.GL_LEQUAL);                           // The Type Of Depth Test To Do
			PVM.setUpProjection(45f, width, height, 0.1f, 100f);
			System.out.println("OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));
			extensions = GL11.glGetString(GL11.GL_EXTENSIONS);

		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean hasExtension(String extension){
		if(extensions == null) return false;
		if(extension == "" || extension == " ") return false;
		if(extensions.contains(extension)) return true;
		return false;
	}
	
	public void loadShaders(){
		
	}

	/**
	 * Called before each frame
	 */
	public void preRender(){

	}

	/**
	 * A frame
	 */
	public void render(){

	}

	/**
	 * Called after each frame
	 */
	public void postRender(){

	}
	
	/**
	 * Called after the render thread is done, serves as a clean up for any bound resources.
	 */
	public void cleanUp(){
		
	}

	/**
	 * The loop for the rendering
	 */
	@Override
	public void run() {
		init();
		Game.threadLoaded();
		waitThis();
		//Finished setup
		
		while(!done){
			if(jobsWaiting){
				jobManager.processJobs();
			}else if (startFrame){
				preRender();
				render();
				postRender();
			}else{
				waitThis();
			}			
		}
		cleanUp();
	}

	/**
	 * Adds a GL Job to perform within the rendering context
	 * @param job The GL job to do
	 */
	public void addJob(GLJob job){
		jobManager.addJob(job);
		notifyThis();
	}
	
	public void addJobs(GLJob[] jobs){
		jobManager.addJobs(jobs);
		notifyThis();
	}
	
	public void notifyThis(){
		try{
			synchronized (this) {
				this.notify();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void waitThis(){
		try {
			synchronized (this) {
				this.wait();
			}			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
