package me.tempus.src;

/**
 * 
 * @author Chris
 * Upper class that manages the lower game states
 */

public class Logic implements Runnable{

	
	
	private boolean done = false;

	@Override
	public void run() {
		// TODO Auto-generated method stub
		init();
		Game.threadLoaded();
		this.waitThis();
		while(!done ){
			update();
			startRenderFrame();
		}
	}
	
	private void init() {
		// TODO Auto-generated method stub
		
	}

	public void update(){
		
		
		
	}
	
	public void startRenderFrame(){
		
	}
	
	public void notifyThis(){
		try{
			synchronized (this) {
				this.notify();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void waitThis(){
		try {
			synchronized (this) {
				this.wait();
			}			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
