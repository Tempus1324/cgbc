/**
 * 
 */
package me.tempus.src;

/**
 * @author Chris
 * The class to manage the level.
 * Like, updating each of platforms and pushing them to the renderer
 * Also things like determining what needs to do move, or any updates
 */
public class Level {

	public Level(){
		
	}
	
	public void init(){
		
	}
	
	public void preUpdate(){
		
	}
	
	public void update(){
		
	}
	
	public void postUpdate(){
		
	}
	
	/**
	 * Setups up a grid of platforms
	 * @param width The amount of pieces in each row
	 * @param height The amount of pieces in each column
	 * @param xPad The x padding between each piece
	 * @param yPad The y padding between each piece
	 */
	public void setUpGrid(int width, int height, int xPad, int yPad){
		final float y = 0;
		
		for(int i = 0; i < width; i++){
			for(int j = 0; j < height; j++){
				
			}
		}
	}
}
