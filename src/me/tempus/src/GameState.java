package me.tempus.src;

/**
 * 
 * @author Chris
 * Interface for a game state. I.E Main Menu, in game, pause menu, end game etc
 */
public interface GameState {
	
	public abstract void preUpdate();
	
	public abstract void update();

	public abstract void postUpdate();
}
