/**
 * 
 */
package me.tempus.test;

import org.lwjgl.opengl.GL20;

import me.tempus.shader.Shader;

/**
 * @author Chris
 *
 */
public class BasicShader extends Shader {

	public BasicShader(String vertPath, String fragPath) {
		super(vertPath, fragPath);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see me.tempus.util.Shader#setUniformLocations()
	 */
	@Override
	public void getUniformLocations() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see me.tempus.util.Shader#bindLocations()
	 */
	@Override
	public void bindLocations() {
		// TODO Auto-generated method stub
		GL20.glBindAttribLocation(shaderID, 0, "in_Position");
		//System.out.println(GL20.glGetAttribLocation(shaderID, "in_Position"));
		//GL20.glBindAttribLocation(shaderProgram, 1, "in_Colour");
		//System.out.println(GL20.glGetAttribLocation(shaderProgram, "in_Colour"));
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Base";
	}

}
