package me.tempus.test;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL33.*;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import me.tempus.modelutil.ImpModel;
import me.tempus.modelutil.ModelLoader;
import me.tempus.shader.ADSShader;
import me.tempus.shader.Shader;
import me.tempus.util.Camera;
import me.tempus.util.Model;
import me.tempus.util.PVM;
import me.tempus.util.Utilities;
import me.tempus.util.VAORender;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import org.newdawn.slick.opengl.Texture;

public class Main {

	public static Main main;

	private static boolean fullscreen = false;
	private static boolean done = false;
	private static int width = 1000;
	private static int height = 1000;
	private final static String TITLE = "Shader primative test";

	//private int shaderProgram = -1;
	//private int vertHandle = -1;
	//private int fragHandle = -1;

	private int samplerLoc;

	private VAORender vao;
	private Texture textureTest;

	private float xRot = 0f;
	private int translate = -1;

	private ImpModel model;
	
	private int testVAO;
	private int testIndicesID;
	private int testIndicesLength;
	
	private ADSShader shader;
	Vector3f lightPosistion = new Vector3f(0,0,0);

	public Main(){
		main = this;


	}

	public void run(){
		try{
			createWindow();
			//setUpShaders();
			shader = new ADSShader("Shaders" + File.separatorChar + "ADS.vert", "Shaders" + File.separatorChar + "ADS.frag");
			shader.loadShaders();
			init();

			Keyboard.enableRepeatEvents(true);
			while(!done){
				update();
				GL20.glUseProgram(shader.getShaderID());
				render();
				
				Display.update();
				Display.sync(60);
				GL20.glUseProgram(0);
			}
			cleanup();
		}catch (Exception e){
			e.printStackTrace();
			System.exit(0);
		}
		System.exit(0);

	}

	public void init(){
		GL11.glEnable(GL11.GL_TEXTURE_2D);                          // Enable Texture Mapping
		GL11.glShadeModel(GL11.GL_SMOOTH);                          // Enables Smooth Color Shading
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);                // This Will Clear The Background Color To Black
		GL11.glClearDepth(1.0);                                   // Enables Clearing Of The Depth Buffer
		GL11.glEnable(GL11.GL_DEPTH_TEST);                          // Enables Depth Testing
		GL11.glDepthFunc(GL11.GL_LEQUAL);                           // The Type Of Depth Test To Do
		PVM.setUpProjection(45f, width, height, 0.1f, 100f);
		System.out.println("OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));
		
		//GL11.glEnable(GL11.GL_CULL_FACE);
		//GL11.glCullFace(GL11.GL_BACK);

		//GL11.glEnable(GL11.GL_LIGHTING);                            // Enable Lighting
		//glColorMaterial(GL_FRONT, GL11.GL_AMBIENT_AND_DIFFUSE);
		//GL11.glEnable(GL11.GL_COLOR_MATERIAL);                      // Enable Material Coloring
		
		/*
		List<Float[]> floats = new ArrayList<Float[]>();
		Float[] test = new Float[]{
				0f, 0.5f, 0f,
				-0.5f, -0.5f, 0.5f,
				0.5f, -0.5f, 0.5f,

				0f, 0.5f, 0f,
				0.5f, -0.5f, -0.5f,
				-0.5f, -0.5f, -0.5f,

				0f, 0.5f, 0f,
				-0.5f, -0.5f, -0.5f,
				-0.5f, -0.5f, 0.5f,

				0f, 0.5f, 0f,
				0.5f, -0.5f, 0.5f,
				0.5f, -0.5f, -0.5f};
		
		floats.add(test);
		testVAO = VAORender.setUp(floats, new int[]{3});
		*/				

		//textureTest = loadTexture("JPG", "Textures" + File.separator + "golddiag");
		//GL11.glBindTexture(GL_TEXTURE_2D, 0);

		//glMatrixMode(GL_PROJECTION);
		//glLoadIdentity();
		//GLU.gluPerspective(45.0f,
		//(float) Display.getWidth() / (float) Display.getHeight(),
		// 0.1f, 100.0f);
		//glOrtho(0, width, 0, height, 1, -1);

		//GL11.glMatrixMode(GL11.GL_MODELVIEW);                       // Select The Modelview Matrix
		//GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);

		
		//getMatrix(GL_PROJECTION_MATRIX);
		
		//PVM.setUpOrg(0, width, 0, height, 1, -1);
		setUpModels();

	}

	public void setUpModels(){
		System.out.println("Starting model load");
		model = ModelLoader.deserialModel("Models" + File.separator + "bike.obj");
		model.loadTexture("PNG", "Data" + File.separatorChar + "rotated-squares");
		System.out.println("Finished model load");
	}

	public void update(){
		while(Keyboard.next()){
			if(Keyboard.getEventKeyState()){
				if(Keyboard.getEventKey() == Keyboard.KEY_LEFT){
					xRot -= 15f;
				}
				else if(Keyboard.getEventKey() == Keyboard.KEY_RIGHT){
					xRot += 15f;
				}
				if(Keyboard.getEventKey() == Keyboard.KEY_UP){
					translate += 1;
				}else if(Keyboard.getEventKey() == Keyboard.KEY_DOWN){
					translate -= 1;
				}
				if(Keyboard.getEventKey() == Keyboard.KEY_G){
					
				}
			}
		}
	}

	public void render(){
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		shader.setLightPosistion(new Vector3f(0,0,0));
		PVM.loadIdentity();
		PVM.translate(0, 0, translate);
		PVM.rotate(0, xRot, 0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		model.draw();
		//GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		
	}

	private void createWindow() throws Exception {
		PixelFormat pixelFormat = new PixelFormat();
		ContextAttribs contextAtrributes = new ContextAttribs(3, 3);
		contextAtrributes.withForwardCompatible(true);
		contextAtrributes.withProfileCore(true);

		Display.setFullscreen(fullscreen);
		Display.setDisplayMode(new DisplayMode(width, height));
		Display.setTitle(TITLE);
		Display.create(pixelFormat, contextAtrributes);
		//Display.create();
		GL11.glViewport(0, 0, width, height);
	}

	public void cleanup(){
		GL20.glDeleteProgram(shader.getShaderID());
		GL20.glDeleteShader(shader.getVertID());
		GL20.glDeleteShader(shader.getFragID());
		Display.destroy();
	}

	public Texture loadTexture(String key, String file){
		Texture texture = Utilities.getTexture(key, file);
		GL11.glBindTexture(GL_TEXTURE_2D, texture.getTextureID());

		GL11.glBindTexture(GL_TEXTURE_2D, 0);

		return texture;
	}

	public void getMatrix(int GLmatrix){
		FloatBuffer test = BufferUtils.createFloatBuffer(16);
		glGetFloat(GLmatrix, test);
		float[] one = new float[test.capacity()];
		test.get(one);
		for(int i = 0; i < one.length; i++){
			System.out.println(i + ": " + one[i]);
		}
		System.out.println("Done");
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Main().run();
	}

	/*
	private void setUpShaders(){
		//Setup shaders
		shaderProgram = GL20.glCreateProgram();
		vertHandle = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
		fragHandle = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);

		StringBuilder[] shaderSources = Utilities.loadShaders(
				"Shaders" + File.separatorChar + "vertShader.vert", "Shaders" + File.separatorChar + "fragShader.frag");

		GL20.glShaderSource(vertHandle, shaderSources[0]);

		GL20.glCompileShader(vertHandle);

		if((GL20.glGetShader(vertHandle, GL20.GL_COMPILE_STATUS)) == GL11.GL_FALSE){
			System.err.println("Compling of vertex shader failed");

		}


		GL20.glShaderSource(fragHandle, shaderSources[1]);
		GL20.glCompileShader(fragHandle);

		if((GL20.glGetShader(fragHandle, GL20.GL_COMPILE_STATUS)) == GL11.GL_FALSE){
			System.err.println("Compling of fragment shader failed");
		}


		GL20.glAttachShader(shaderProgram, vertHandle);
		GL20.glAttachShader(shaderProgram, fragHandle);

		GL20.glBindAttribLocation(shaderProgram, 0, "in_Position");
		//GL20.glBindAttribLocation(shaderProgram, 1, "in_Colour");
		//GL20.glBindAttribLocation(shaderProgram, 2, "in_TextureCoord");

		
		
		GL20.glLinkProgram(shaderProgram);
		GL20.glValidateProgram(shaderProgram);

		//System.out.println(GL20.glGetAttribLocation(shaderProgram, "in_Position"));
		//System.out.println("Atrribute index 0: " + GL20.glGetActiveAttrib(shaderProgram, 0, 1000));
		
		int projectionMatrixLocation = GL20.glGetUniformLocation(shaderProgram, "projection_matrix");
		int viewMatrixLocation = GL20.glGetUniformLocation(shaderProgram, "view_matrix");
		int modelMatrixLocation = GL20.glGetUniformLocation(shaderProgram, "model_matrix");

		PVM.setLocations(projectionMatrixLocation, viewMatrixLocation, modelMatrixLocation);

		samplerLoc = GL20.glGetUniformLocation(shaderProgram, "texture_diffuse");
		
		if((GL20.glGetProgram(shaderProgram, GL20.GL_LINK_STATUS) == GL_FALSE)){
				
			System.out.println("Link failed");
		}
		
		System.out.println("GL_ACTIVE_ATTRIBUTES: " + GL20.glGetProgram(shaderProgram, GL20.GL_ACTIVE_ATTRIBUTES));
		
		System.out.println(GL20.glGetShaderInfoLog(fragHandle, 1000));
		System.out.println(GL20.glGetShaderInfoLog(vertHandle, 1000));
	}
*/

}
