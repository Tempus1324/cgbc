/**
 * 
 */
package me.tempus.math;

/**
 * @author Chris
 *
 */
public class MatrixMath {

	/**
	 * @param matrix
	 * @return The coefficients of the characteristic polynomial of a 3x3 matrix
	 */
	public static float[] cp3(float[] matrix){
		final float[] values = new float[4];
		values[0] = -1;
		final float trace = trace3(matrix);
		values[1] = trace;
		values[2] = -((trace * trace) - (trace3(matrixSquare(matrix, 3, 3))))/2;
		//values[3] = (trace * trace * trace)
				//+ (2 * trace3(matrixPow3(matrix, 3, 3)))
				//- (3 * trace * trace3(matrixSquare(matrix, 3, 3)));
		values[3] = det3(matrix);
		return values;
	}

	/**
	 * Determinate for a 3x3 matrix
	 * @param m
	 * @return
	 */
	public static float det3(float[] m){
		return (m[0]*m[4]*m[8]) - (m[0]*m[5]*m[7]) - (m[1]*m[3]*m[8]) + (m[1]*m[5]*m[6]) + (m[2]*m[3]*m[7]) - (m[2]*m[4]*m[6]);
	}
	
	/**
	 * Sqaurea a matrix
	 * @param matrix
	 * @param n Row size
	 * @param m Column Size
	 * @return
	 */
	public static float[] matrixSquare(float[] matrix, int n, int m){
		final float[] result = new float[matrix.length];
		
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				result[getPosistion(i, j, m)] = getMatrixSum(matrix, matrix, m, m, i, j);
			}			
		}
		
		return result;
	}
	
	/**
	 * Raise a matrix to the power of 3
	 * @param matrix Matrix
	 * @param pow The power to raise the matrix to
	 * @param n Row size of the matrix
	 * @param m Column size of the matrix
	 * @return The product matrix
	 */
	public static float[] matrixPow3(float[] matrix, int n, int m){
		float[] result = new float[matrix.length];

		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				result[getPosistion(i, j, m)] = getMatrixSum(matrix, matrix, m, m, i, j);
			}			
		}
		final float[] resultTwo = new float[matrix.length];
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				resultTwo[getPosistion(i, j, m)] = getMatrixSum(result, matrix, m, m, i, j);
			}			
		}
		return resultTwo;
	}
		/**
		 * Trace of a nxn size matrix
		 * @param matrix A square matrix
		 * @param n Size of the matrix
		 * @return The trace of the matrix
		 */
		public static float trace(float[] matrix, int n){
			float trace = 0;
			for(int i = 0; i < n; i++){
				trace += matrix[getPosistion(i, i, n)];
			}
			return trace;
		}

		/**
		 * @param matrix
		 * @return Trace for a 3x3 matrix
		 */
		public static float trace3(float[] matrix){
			return matrix[0] + matrix[4] + matrix[8];
		}

		/**
		 * @param matrix
		 * @return Trace for a 2x2 matrix
		 */
		public static float trace2(float[] matrix){
			return matrix[0] + matrix[3];
		}

		/**
		 * TODO Test
		 * @param matrix1 Matrix 1
		 * @param matrix2 Matrix 2
		 * @param n Matrix1 row size
		 * @param m Matrix1 column size
		 * @param o Matrix2 row size
		 * @param p Matrix2 column size
		 * @return The product matrix
		 */
		public static float[] mulitpleMatrix(float[] matrix1, float[] matrix2, int n, int m, int o, int p){
			assert m == o;
			final float[] f = new float[m*p];

			for(int i = 0; i < m; i++){
				for(int j = 0; j < p; j++){
					f[getPosistion(i, j, p-1)] = getMatrixSum(matrix1, matrix2, m, p, i, j);
				}
			}

			return f;
		}

		/**
		 * Used in multiplying matrices. Basically the cross product
		 * @param matrix1
		 * @param matrix2
		 * @param m Matrix1 column size
		 * @param p Matrix2 column size
		 * @param i Current row position of the product matrix
		 * @param j Current column position of the product matrix
		 * @return
		 */
		public static float getMatrixSum(float[] matrix1, float[] matrix2, int m, int p, int i, int j){
			float returnValue = 0;
			for(int k = 0; k < m; k++){
				returnValue += matrix1[getPosistion(i, k, m)] * matrix2[getPosistion(k, j, p)];
			}
			return returnValue;
		}

		/**
		 * Reduced row echelon form
		 * @param matrix The augmented matrix to solve
		 * @param matrixSize The size the matrix's row
		 * @return Returns the solved matrix
		 */
		public static float[] rref(float[] matrix, int matrixSize){
			int i = 0;
			int j = 0;

			for(;j < matrixSize; j++){
				float lastValue = 0;
				int largestRow = i;
				for(int k = i; k < matrixSize; k++){
					final int pos = getPosistion(k, j, matrixSize+1);
					if(Math.abs(matrix[pos]) > lastValue){
						lastValue = matrix[pos];
						largestRow = k;
					}
				}
				/**
				 * If found largest value in a row, swamp the row
				 */
				if(largestRow != i){
					matrix = swapRows(matrix, matrixSize +1, i, largestRow);
				}

				/**
				 * Multiple row i by 1/mij
				 */
				matrix = multipleRow(matrix, matrixSize+1, i, 1 / matrix[getPosistion(i, j, matrixSize+1)]);

				/**
				 * For each row r, where 1≤r≤n and r≠i, add -Mrj times row i it row r
				 */
				for(int r = 0; r < matrixSize; r++){
					if(r != i){
						final float mrj = -matrix[getPosistion(r, j, matrixSize+1)];
						for(int l = 0; l < matrixSize+1; l++){
							matrix[getPosistion(r, l, matrixSize+1)] += mrj * matrix[getPosistion(i, l, matrixSize+1)];
						}
					}
				}
				i++;
			}

			return matrix;
		}


		/**
		 * Gets the last column of a matrix
		 * Useful for getting the result of an augmented matrix via rref
		 * @param matrix
		 * @param rowSize
		 * @param columnSize
		 * @return
		 */
		public static float[] getLastColumn(float[] matrix, int rowSize, int columnSize){
			//TODO Test
			final float[] newMatrix = new float[rowSize];

			for(int i = 0; i < rowSize; i++){
				newMatrix[i] = ((i+1) * columnSize) -1;
			}

			return newMatrix;
		}

		/**
		 * Values are zero indexed
		 * @param matrix
		 * @param matrixSize Column size of the matrix
		 * @param row1 Row one to be swaped with
		 * @param row2 Row two to swap to row one
		 * @return
		 */
		public static float[] swapRows(float[] matrix, int matrixSize, int row1, int row2){

			for(int i = 0; i < matrixSize; i++){
				final float m1 = matrix[((matrixSize * row1) + i)];
				final float m2 = matrix[((matrixSize * row2) + i)];

				matrix[(matrixSize * row1) + i] = m2;
				matrix[(matrixSize * row2) + i] = m1;			
			}

			return matrix;
		}

		public static float[] multipleRow(float[] matrix, int columnSize, int row, float scalar){

			for(int i = 0; i < columnSize; i++){
				matrix[getPosistion(row, i, columnSize)] *= scalar;
			}

			return matrix;
		}

		public static float[] addRow(float[] matrix, int matrixSize, int row, float scalar){
			for(int i = 0; i < matrixSize; i++){
				matrix[(matrixSize * row) + i] += scalar;
			}

			return matrix;
		}

		/**
		 * All values expect matrixSize must be zero indexed
		 * @param i
		 * @param j
		 * @param matrixSize The column size of the matrix, <b> must <u>not</u> be zero indexed </b>
		 * @return The zero indexed posistion of the value in the matrix
		 */
		public static int getPosistion(int i, int j, int matrixSize){
			final int k = ((matrixSize * i) + j);
			return k;
		}
	}
