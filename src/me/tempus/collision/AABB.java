package me.tempus.collision;

import me.tempus.math.Vector3f;

public class AABB {

	public static AABV generateAABB(Vector3f[] vertices, int[] indices){
		Vector3f max = new Vector3f(0,0,0);
		Vector3f min = new Vector3f(0,0,0);
		
		final int size = indices.length;
		for(int i = 0; i < size; i++){
			final Vector3f v = vertices[indices[i]];
			if(v.x <= min.x){
				if(v.y <= min.y){
					if(v.z <= min.z){
						min = v;
					}
				}
			}else if(v.x >= max.x){
				if(v.y >= max.y){
					if(v.z >= max.z){
						max = v;
					}
				}
			}
		}
		System.out.println("Max: " + max + " Min: " + min);
		final Vector3f c = new Vector3f((min.x + max.x) /2, (min.y + max.y) /2, (min.z + max.z) /2);
		final Vector3f halfExtents = new Vector3f((max.x - min.x) /2, (max.y - min.y) /2, (max.z - min.z) /2);
		
		return new AABV(halfExtents, c);
	}
	
	/**
	 * 
	 * @param a
	 * @return Object array, 0: vertices 1: indices
	 */
	public static Object[] getRenderable(AABV a){
		
		final Vector3f c = a.getCentre();
		final Vector3f r = a.getHalfExtents();
		final float closeY = c.y - r.y;
		final float farY = c.y + r.y;
		final float[] vertices = new float[]{
				c.x - r.x, closeY, c.z + r.z,
				c.x - r.x, closeY, c.z - r.z,
				c.x + r.x, closeY, c.z - r.z,
				c.x + r.x, closeY, c.z + r.z,
				c.x - r.x, farY, c.z - r.z,
				c.x + r.x, farY, c.z + r.z,
				c.x + r.x, farY, c.z - r.z,
				c.x - r.x, farY, c.z - r.z
		};
		
		final int[] indicies = new int[]{
				0,1,2,	2,3,0,
				4,5,6,	6,7,4,
				0,3,5,  5,4,0,
				3,2,6,	6,5,3,
				2,1,7,	7,6,2,
				1,0,4,	4,7,1
		};
		
		return new Object[]{vertices, indicies};
	}
	
}


