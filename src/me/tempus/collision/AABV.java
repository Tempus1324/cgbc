package me.tempus.collision;

import me.tempus.math.Vector3f;

public class AABV {

	private Vector3f halfExtents;
	private Vector3f centre;
	/**
	 * @param halfExtents
	 * @param centre
	 */
	public AABV(Vector3f halfExtents, Vector3f centre) {
		super();
		this.halfExtents = halfExtents;
		this.centre = centre;
	}
	
	@Override
	public String toString(){
		return "Centre: " + centre + " HalfExtents: " + halfExtents;
	}

	/**
	 * @return the halfExtents
	 */
	public Vector3f getHalfExtents() {
		return halfExtents;
	}

	/**
	 * @param halfExtents the halfExtents to set
	 */
	public void setHalfExtents(Vector3f halfExtents) {
		this.halfExtents = halfExtents;
	}

	/**
	 * @return the centre
	 */
	public Vector3f getCentre() {
		return centre;
	}

	/**
	 * @param centre the centre to set
	 */
	public void setCentre(Vector3f centre) {
		this.centre = centre;
	}
	
	
}
