/**
 * 
 */
package me.tempus.shader;

import me.tempus.modelutil.ImpModel;

import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

/**
 * @author Chris
 *
 */
public class ADSShader extends Shader {
	
	private int vLightPosition;
	private int ambientColorID;
	private int diffuseColorID;
	private int specularColorID;
	private Vector3f lightPosistion;
	private int shineID;
	private int lightAmbientID;
	private int lightDiffuseID;
	private int lightSpecularID;
	
	private Vector4f ambient;
	private Vector4f diffuse;
	private Vector4f specular;
	
	private int samplerLoc;
	//Vector4f ambientColor = new Vector4f(1f, 1f, 1f, 1f);
	//Vector3f lightPosistion = new Vector3f(0,0,0);
	
	public ADSShader(String vertPath, String fragPath) {
		super(vertPath, fragPath);
		// TODO Auto-generated constructor stub
	}

	
	public void passUniforms(float[] ambientColor, float[] diffuseColor, float[] specularColor, float shine) {
		// TODO Auto-generated method stub
		GL20.glUniform4f(ambientColorID, ambientColor[0], ambientColor[1], ambientColor[2], 1);
		GL20.glUniform4f(diffuseColorID, diffuseColor[0], diffuseColor[1], diffuseColor[2], 1);
		GL20.glUniform4f(specularColorID, specularColor[0], specularColor[1], specularColor[2], 1);
		GL20.glUniform3f(vLightPosition, lightPosistion.x, lightPosistion.y, lightPosistion.z);
		//GL20.glUniform3f(lightAmbientID, ambient.x, ambient.y, ambient.z);
		//GL20.glUniform3f(lightDiffuseID, diffuse.x, diffuse.y, diffuse.z);
		//GL20.glUniform3f(lightSpecularID, specular.x, specular.y, specular.z);
		GL20.glUniform1f(shineID, shine);
		GL20.glUniform1i(samplerLoc, 0);
	}
	
	public void setLightPosistion(Vector3f pos){
		this.lightPosistion = pos;
	}
	
	@Override
	public void getUniformLocations() {
		// TODO Auto-generated method stub		
		vLightPosition = GL20.glGetUniformLocation(shaderID, "vLightPosition");
		ambientColorID = GL20.glGetUniformLocation(shaderID, "ambientColor");
		diffuseColorID = GL20.glGetUniformLocation(shaderID, "diffuseColor");
		specularColorID = GL20.glGetUniformLocation(shaderID, "specularColor");
		shineID = GL20.glGetUniformLocation(shaderID, "shine");
		lightAmbientID = GL20.glGetUniformLocation(shaderID, "lightAmbient");
		lightDiffuseID = GL20.glGetUniformLocation(shaderID, "lightDiffuse");
		lightSpecularID = GL20.glGetUniformLocation(shaderID, "lightSpecular");
		samplerLoc = GL20.glGetUniformLocation(shaderID, "colorMap");
	}

	@Override
	public void bindLocations() {
		GL20.glBindAttribLocation(shaderID, 0, "vVertex");
		GL20.glBindAttribLocation(shaderID, 1, "vNormal");		
		GL20.glBindAttribLocation(shaderID, 2, "vTexture");
	}


	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "ADS";
	}
	
	

}
