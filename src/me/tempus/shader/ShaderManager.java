package me.tempus.shader;

import java.util.HashMap;

public class ShaderManager {

	private static HashMap<String, Shader> shaders = new HashMap<String, Shader>();
	
	public static Shader getShader(String shaderName){
		return shaders.get(shaderName);
	}
	
	public static void loadShader(String name, Shader shader){
		shaders.put(name, shader);
	}
	
}
