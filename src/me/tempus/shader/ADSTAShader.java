/**
 * 
 */
package me.tempus.shader;

import me.tempus.modelutil.ADSTAModel;

import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;


/**
 * @author Chris
 *
 */
public class ADSTAShader extends Shader {
	
	private int vLightPosition;
	private int ambientColorID;
	private int diffuseColorID;
	private int specularColorID;
	private int textureIndexID;
	private Vector3f lightPosistion;
	private int shineID;
	private int lightAmbientID;
	private int lightDiffuseID;
	private int lightSpecularID;
	
	private Vector4f ambient;
	private Vector4f diffuse;
	private Vector4f specular;
	private int samplerID;
	
	
	
	//Vector4f ambientColor = new Vector4f(1f, 1f, 1f, 1f);
	//Vector3f lightPosistion = new Vector3f(0,0,0);
	
	public ADSTAShader(String vertPath, String fragPath) {
		super(vertPath, fragPath);
		// TODO Auto-generated constructor stub
		this.lightPosistion = new Vector3f(0,0,1);
	}

	
	public void passUniforms(float[] ambientColor, float[] diffuseColor, float[] specularColor, float shine, float textureIndex) {
		// TODO Auto-generated method stub
		GL20.glUniform4f(ambientColorID, ambientColor[0], ambientColor[1], ambientColor[2], 1);
		GL20.glUniform4f(diffuseColorID, diffuseColor[0], diffuseColor[1], diffuseColor[2], 1);
		GL20.glUniform4f(specularColorID, specularColor[0], specularColor[1], specularColor[2], 1);
		GL20.glUniform3f(vLightPosition, lightPosistion.x, lightPosistion.y, lightPosistion.z);
		//GL20.glUniform3f(lightAmbientID, ambient.x, ambient.y, ambient.z);
		//GL20.glUniform3f(lightDiffuseID, diffuse.x, diffuse.y, diffuse.z);
		//GL20.glUniform3f(lightSpecularID, specular.x, specular.y, specular.z);
		GL20.glUniform1f(shineID, shine);
		GL20.glUniform1f(textureIndexID, textureIndex);
		GL20.glUniform1i(samplerID, 0);
	}
	
	public void setLightPosistion(Vector3f pos, Vector4f ambient, Vector4f diffuse, Vector4f specular){
		this.lightPosistion = pos;
		this.ambient = ambient;
		this.diffuse = diffuse;
		this.specular = specular;
	}
	
	@Override
	public void getUniformLocations() {
		// TODO Auto-generated method stub		
		vLightPosition = GL20.glGetUniformLocation(shaderID, "vLightPosition");
		ambientColorID = GL20.glGetUniformLocation(shaderID, "ambientColor");
		diffuseColorID = GL20.glGetUniformLocation(shaderID, "diffuseColor");
		specularColorID = GL20.glGetUniformLocation(shaderID, "specularColor");
		shineID = GL20.glGetUniformLocation(shaderID, "shine");
		lightAmbientID = GL20.glGetUniformLocation(shaderID, "lightAmbient");
		lightDiffuseID = GL20.glGetUniformLocation(shaderID, "lightDiffuse");
		lightSpecularID = GL20.glGetUniformLocation(shaderID, "lightSpecular");
		textureIndexID = GL20.glGetUniformLocation(shaderID, "textureIndex");
		samplerID = GL20.glGetUniformLocation(shaderID, "sampler");
	}

	@Override
	public void bindLocations() {
		// TODO Auto-generated method stub
		GL20.glBindAttribLocation(shaderID, 0, "vVertex");
		GL20.glBindAttribLocation(shaderID, 1, "vNormal");		
		GL20.glBindAttribLocation(shaderID, 2, "vTexture");
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "ADSTA";
	}
	
	

}
