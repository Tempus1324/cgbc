package me.tempus.modelutil;

import java.util.ArrayList;

import me.tempus.shader.Shader;

public class TexturedMesh extends Mesh {

	private int textureIndex;
	
	public void passMaterial(float[] ambient, float[] diffuse, float[] spectual, float shine, int textureIndex){
		this.ambient = ambient;
		this.diffuse = diffuse;
		this.spectual = spectual;
		this.shine = shine;
		this.textureIndex = textureIndex;
	}
	
	public int getTextureIndex(){
		return textureIndex;
	}
	
	public void setTextureIndex(int index){
		this.textureIndex = index;
	}
}
