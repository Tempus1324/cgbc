/**
 * 
 */
package me.tempus.modelutil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.tempus.util.Face;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 * @author Chris
 *
 */
public class ADSModelLoader extends ModelLoaderTemplate {

	private static ImpModel model;

	private static HashMap<String, Material> materials = new HashMap();

	private static List<Vector3f> currentPosistions = new ArrayList<Vector3f>();
	private static List<Vector3f> currentNormals = new ArrayList<Vector3f>();
	private static List<Vector2f> currentTextureCoords = new ArrayList<Vector2f>();

	private static List<Face> loadedFaces = new ArrayList<Face>();
	private static Face currentFace = null;

	private static int[] indices;

	private static List<Mesh> meshes = new ArrayList<Mesh>();

	private static int debug_nuOfFacePush;

	private boolean loadingFaces;

	public void loadMesh(BufferedReader reader, String folderPath, String file) throws IOException{

		BufferedReader mtlReader;
		model = new ImpModel();

		String currentLine;
		Material currentMaterial;
		int i = 0;

		while((currentLine = reader.readLine()) != null){
			if(currentLine.startsWith("mtllib ")){
				String[] values = currentLine.split(" ");
				mtlReader = new BufferedReader(new FileReader(folderPath + File.separatorChar + values[1]));
				loadMaterialLibrary(mtlReader);
			}else if(currentLine.startsWith("usemtl ")){
				/**
				 * TODO Read below
				 *  Pull down material
				 *  Grab float data out of material and push it to the current mesh
				 *  Push vertices data and load the indices list with the faces
				 */
				pushFaces();
				String[] values = currentLine.split(" ");
				if(values.length >= 2){
					currentMaterial = materials.get(values[1]);
				}else{
					currentMaterial = new Material("");
				}
				//System.err.println("Name: " + values[1]);
				i++;
				currentFace = new Face(currentMaterial);
			}else{
				/**
				 * TODO Read below
				 * If have no other flags
				 * Pass line to readLine to load the data, should be a compontent of the vertex 
				 */
				readLine(currentLine);
			}
		}
		if(loadingFaces){
			loadingFaces = false;
			pushFaces();
		}
		System.out.println("Number of materials: " + i);
		System.out.println("Number of Faces: " + loadedFaces.size());
		generateMeshes();
		System.out.println("Number of meshes: " + meshes.size());
		reader.close();
		model.setMeshes(meshes);
		
		writeToFile("Models", file);

		System.out.println("Done");
	}

	@Override
	public void loadMaterialLibrary(BufferedReader reader) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub
		String currentLine;
		Material currentMaterial = null;
		while((currentLine = reader.readLine()) != null){
			/**
			 * TODO Read below
			 * Read through material file and load the different materials
			 * Push materials to HashMap of materials and there names
			 */
			if(currentLine.startsWith("newmtl ")){
				String[] values = currentLine.split(" ");
				if(values.length == 2){
					currentMaterial = new Material(values[1]);
				}else{
					currentMaterial = new Material("");
				}
				if(currentMaterial != null){
					materials.put(currentMaterial.getName(), currentMaterial);
				}
			}else if(currentLine.startsWith("Ka ")){
				String[] values = currentLine.split(" ");
				float r = Float.valueOf(values[1]);
				float g = Float.valueOf(values[2]);
				float b = Float.valueOf(values[3]);
				currentMaterial.setAmbient(new float[]{r, g, b});
			}else if(currentLine.startsWith("Ks ")){
				String[] values = currentLine.split(" ");
				float r = Float.valueOf(values[1]);
				float g = Float.valueOf(values[2]);
				float b = Float.valueOf(values[3]);
				currentMaterial.setSpectual(new float[]{r, g, b});
			}else if(currentLine.startsWith("Kd ")){
				String[] values = currentLine.split(" ");
				float r = Float.valueOf(values[1]);
				float g = Float.valueOf(values[2]);
				float b = Float.valueOf(values[3]);
				currentMaterial.setDiffuse(new float[]{r, g, b});
			}else if(currentLine.startsWith("Ns ")){
				String[] values = currentLine.split(" ");
				float shine = Float.valueOf(values[1]);
				currentMaterial.setShine(shine);
			}
		}
		reader.close();

	}

	@Override
	public void generateMeshes() {
		// TODO Auto-generated method stub
		for(Face currentFace: loadedFaces){
			Mesh newMesh = loadIndices(currentFace.getLoadedFaces(), currentFace.getFaces());
			/**
			 * Uncomment to see what is loading, created to copy box data into static hard code
			final float[] v = newMesh.getVertices();
			System.out.println("Vertices:");
			for(int i = 0; i < v.length; i++){
				System.out.print(v[i] + " ");
			}
			
			final float[] n = newMesh.getNormals();
			System.out.println("Normals:");
			for(int i = 0; i < n.length; i++){
				System.out.print(n[i]+ " ");
			}
			
			final float[] t = newMesh.getTextureUV();
			System.out.println("Texture:");
			for(int i = 0; i < t.length; i++){
				System.out.print(t[i]+ " ");
			}
			
			final int[] in = newMesh.getIndicies();
			System.out.println("Indicies:");
			for(int i = 0; i < in.length; i++){
				System.out.print(in[i]+ " ");
			}
			*/
			Material meshMaterial = currentFace.getMaterial();
			newMesh.passMaterial(meshMaterial.getAmbient(), meshMaterial.getDiffuse(), meshMaterial.getSpectual(), meshMaterial.getShine());
			meshes.add(newMesh);
		}
	}

	@Override
	public void pushFaces() {
		// TODO Auto-generated method stub
		if(!(loadedFaces.contains(currentFace)) && currentFace != null){
			loadedFaces.add(currentFace);
		}
	}

	@Override
	public void readLine(String line) {
		// TODO Auto-generated method stub
		if(line.startsWith("v ")){
			String[] values = line.split(" ");
			float x = Float.valueOf(values[1]);
			float y = Float.valueOf(values[2]);
			float z = Float.valueOf(values[3]);
			Vector3f pos = new Vector3f(x, y, z);
			currentPosistions.add(pos);
		}else if(line.startsWith("vn ")){
			String[] values = line.split(" ");
			float x = Float.valueOf(values[1]);
			float y = Float.valueOf(values[2]);
			float z = Float.valueOf(values[3]);
			Vector3f normal = new Vector3f(x, y, z);
			currentNormals.add(normal);
		}else if(line.startsWith("vt ")){
			String[] values = line.split(" ");
			float x = Float.valueOf(values[1]);
			float y = Float.valueOf(values[2]);
			Vector2f textureCoord = new Vector2f(x, y);
			currentTextureCoords.add(textureCoord);
		}else if(line.startsWith("f ")){
			String[] values = line.split(" ");
			loadingFaces = true;
			currentFace.loadFace(values[1]);
			currentFace.loadFace(values[2]);
			currentFace.loadFace(values[3]);			
		}else if(!(line.startsWith("f ")) && loadingFaces){
			// Stop loading faces, reached end of the current Mesh
			loadingFaces = false;
			//loadIndices(currentFaces.size(), currentMesh);
			debug_nuOfFacePush++;
			System.out.println("Number of face pushes: " + debug_nuOfFacePush);
			pushFaces();
		}
	}

	@Override
	public Mesh loadIndices(HashMap<String, Integer> faces,
			List<String> currentFaces) {
		Mesh currentMesh = new Mesh();
		indices = new int[currentFaces.size()];
		int amountOfLoadedFaces = faces.size();
		float[] posistions = new float[amountOfLoadedFaces * 3];
		float[] normals = new float[amountOfLoadedFaces * 3];
		float[] textureCoords = new float[amountOfLoadedFaces * 2];

		String[] facesArray = (faces.keySet()).toArray(new String[amountOfLoadedFaces]);

		for(int i = 0; i < amountOfLoadedFaces; i++){
			String faceString = facesArray[i];
			String[] faceValues = faceString.split("/");
			int currentPos = faces.get(faceString);

			Vector3f posistion = currentPosistions.get(Integer.parseInt(faceValues[0]) -1);
			posistions[(currentPos * 3)] = posistion.x;
			posistions[(currentPos * 3) +1] = posistion.y;
			posistions[(currentPos * 3) +2] = posistion.z;

			if(!(faceValues[1].equalsIgnoreCase(""))){
				Vector2f textureCoord = currentTextureCoords.get(Integer.parseInt(faceValues[1]) -1);
				textureCoords[(currentPos *2)] = textureCoord.x;
				textureCoords[(currentPos *2) +1] = textureCoord.y;
			}

			Vector3f normal = currentNormals.get(Integer.parseInt(faceValues[2]) -1);
			normals[(currentPos *3)] = normal.x;
			normals[(currentPos *3) +1] = normal.y;
			normals[(currentPos *3) +2] = normal.z;

		}

		for(int i = 0; i < currentFaces.size(); i++){
			int index = faces.get(currentFaces.get(i));
			indices[i] = index;
		}

		currentMesh.passFloats(posistions, normals, textureCoords, indices);
		return currentMesh;

	}

	public void writeToFile(String folderPath, String path){
		FileOutputStream fos;
		try {
			System.out.println("Writing to: " + folderPath + File.separator + path);
			fos = new FileOutputStream(folderPath + File.separator + path);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(model);
			oos.flush();
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

}
