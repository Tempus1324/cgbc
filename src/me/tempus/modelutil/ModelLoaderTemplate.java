package me.tempus.modelutil;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class ModelLoaderTemplate {

	public abstract void loadMaterialLibrary(BufferedReader reader) throws NumberFormatException, IOException;
	
	public abstract void generateMeshes();
	
	public abstract void pushFaces();
	
	public abstract void readLine(String line);
	
	public abstract void loadMesh(BufferedReader reader, String folderPath, String file) throws IOException;
	
	public abstract Mesh loadIndices(HashMap<String, Integer> faces, List<String> currentFaces);

	public static ImpModel deserialModel(String modelPath){
		Object model = null;


		try {
			FileInputStream fis = new FileInputStream(modelPath); 
			ObjectInputStream ois = new ObjectInputStream(fis);
			model = ois.readObject();
			ois.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}


		return (ImpModel) model;
	}
}
