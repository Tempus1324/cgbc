/**
 * 
 */
package me.tempus.modelutil;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;

import me.tempus.shader.ADSTAShader;
import me.tempus.util.PVM;
import me.tempus.util.Utilities;

/**
 * @author Chris
 *
 */
public class ADSTexturedModel extends ImpModel {

	private ArrayList<TexturedMesh> meshes = new ArrayList<TexturedMesh>();
	private String[] textureFiles;
	private transient int textureName;
	
	public void init(){
		textureName = Utilities.loadTextureArray("", textureFiles, 256, 256, "PNG");
	}
	
	public void draw(){
		GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, textureName);
		for(TexturedMesh mesh: meshes){
			PVM.pushMatrix();
			applyTransformation();
			passData(mesh.getAmbient(), mesh.getDiffuse(), mesh.getSpectual(), mesh.getShine(), mesh.getTextureIndex());
			mesh.draw(shader);
			PVM.popMatrix();
		}
		GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, 0);
	}
	
	public void passData(float[] ambient, float[] diffuse, float[] spectual, float shine, int textureIndex){
		if(shader instanceof ADSTAShader){
			((ADSTAShader) shader).passUniforms(ambient, diffuse, spectual, shine, textureIndex);
		}
	}
	
	public void setTextureFiles(String[] values){
		this.textureFiles = values;
	}
}
