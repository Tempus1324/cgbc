/**
 * 
 */
package me.tempus.modelutil;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.tempus.shader.ADSShader;
import me.tempus.shader.Shader;
import me.tempus.util.PVM;
import me.tempus.util.Utilities;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;

/**
 * @author Chris
 *
 */
public class ImpModel implements Serializable {
	
	public static final long serialVersionUID = 100L;
	
	private List<Mesh> meshes = new ArrayList<Mesh>();
	private transient Texture texture;

	protected Vector3f translate;
	protected Vector3f scale;
	protected Vector3f rotate;
	
	protected static Shader shader;
	
	public void setUpVao(){
		translate = new Vector3f(0, 0, 0);
		scale = new Vector3f(1, 1, 1);
		rotate = new Vector3f(0, 0, 0);
		for(Mesh mesh : meshes){
			mesh.setUpVao();
		}
	}
	
	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException{
		in.defaultReadObject();
		init();
		setUpVao();
	}
	
	public void draw(){	
		for(Mesh mesh: meshes){
			PVM.pushMatrix();
			applyTransformation();
			passData(mesh.getAmbient(), mesh.getDiffuse(), mesh.getSpectual(), mesh.getShine());
			mesh.draw(shader);
			PVM.popMatrix();
		}
	}
	
	public void applyTransformation(){
		PVM.translate(translate.x, translate.y, translate.z);
		PVM.scale(scale.x, scale.y, scale.z);
		PVM.rotate(rotate.x, rotate.y, rotate.z);
	}
	
	public void setMeshes(List<Mesh> meshes){
		this.meshes = meshes;
	}
	
	public void loadTexture(String key, String path) {
		texture = Utilities.getTexture(key, path);		
	}

	/**
	 * @param translate the translate to set
	 */
	public void setTranslate(Vector3f translate) {
		this.translate = translate;
	}

	/**
	 * @param scale the scale to set
	 */
	public void setScale(Vector3f scale) {
		this.scale = scale;
	}

	/**
	 * @param rotate the rotate to set
	 */
	public void setRotate(Vector3f rotate) {
		this.rotate = rotate;
	}

	/**
	 * @return the translate
	 */
	public Vector3f getTranslate() {
		return translate;
	}

	/**
	 * @return the scale
	 */
	public Vector3f getScale() {
		return scale;
	}

	/**
	 * @return the rotate
	 */
	public Vector3f getRotate() {
		return rotate;
	}
	
	public void passData(float[] ambient, float[] diffuse, float[] spectual, float shine){
		if(shader instanceof ADSShader){
			((ADSShader) shader).passUniforms(ambient, diffuse, spectual, shine);
		}
	}
	
	public void init(){}

	/**
	 * @return the textureID
	 */
	public int getTexture() {
		return texture.getTextureID();
	}

	/**
	 * @param texture the texture to set
	 */
	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	/**
	 * @return the meshes
	 */
	public List<Mesh> getMeshes() {
		return meshes;
	}

	public static void setShader(Shader shader){
		ImpModel.shader = shader;
	}
	

	/*
	public void addVertex(float x, float y, float z){
		
		int i = 0;
		if(vertices != null)
			i = vertices.length;
		vertices[i] = x;
		vertices[i + 1] = y;
		vertices[i + 2] = z;
		
		//vertices.add(x);
		//vertices.add(y);
		//vertices.add(z);
		
		vertices[verticesIndex] = x;
		verticesIndex++;
		vertices[verticesIndex] = y;
		verticesIndex++;
		vertices[verticesIndex] = z;
		verticesIndex++;
		
		vertices.put(x);
		vertices.put(y);
		vertices.put(z);
	}
*/
	
/*
	public void addNoraml(float x, float y, float z){
		/*
		int i = 0;
		if(normals != null)
			i = normals.length;
		normals[i] = x;
		normals[i + 1] = y;
		normals[i + 2] = z;
		
		//normals.add(x);
		//normals.add(y);
		//normals.add(z);
		/*
		normals[normalIndex] = x;
		normalIndex++;
		normals[normalIndex] = y;
		normalIndex++;
		normals[normalIndex] = z;
		normalIndex++;
		
		normals.put(x);
		normals.put(y);
		normals.put(z);
	}
	*/
	
	/*
	public static float[] tofloatArray(Float[] values){
		float[] floatArray = new float[values.length];

		for (int i = 0; i < values.length; i++) {
		    Float f = values[i];
		    floatArray[i] = (f != null ? f : Float.NaN); // Or whatever default you want.
		}
		return floatArray;
	}
	*/

	/*
	public void addTextureUV(float x, float y) {
		// TODO Auto-generated method stub
		//textureUV.add(x);
		//textureUV.add(y);
		/*
		textureUV[textureUVIndex] = x;
		textureUVIndex++;
		textureUV[textureUVIndex] = y;
		textureUVIndex++;
		
		textureUV.put(x);
		textureUV.put(y);
	}
*/


	
	
}
