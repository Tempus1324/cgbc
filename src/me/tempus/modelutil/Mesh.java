/**
 * 
 */
package me.tempus.modelutil;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import me.tempus.jobs.GLJob;
import me.tempus.jobs.GLJob_SetUpVOA;
import me.tempus.jobs.GLJobi;
import me.tempus.levelediter.Render;
import me.tempus.shader.Shader;
import me.tempus.util.PVM;
import me.tempus.util.Utilities;
import me.tempus.util.VAORender;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

/**
 * @author Chris
 *
 */
public class Mesh implements Serializable, GLJobi {
	
	public static final long serialVersionUID = 100L;
	
	private float[] vertices;
	private float[] normals;
	private float[] textureUV;
	private int[] indicies;
	
	protected float[] ambient;
	protected float[] diffuse;
	protected float[] spectual;
	protected float shine;
	
	private Vector3f translate = new Vector3f(0,0,0);
	private Vector3f scale = new Vector3f(1, 1, 1);
	private Vector3f rotate = new Vector3f(0,0,0);
	
	private int vaoID = -1;
	private int indiceID;

	
	public void setUpVao(){
		translate = new Vector3f(0,0, 0);
		scale = new Vector3f(1, 1, 1);
		rotate = new Vector3f(0,0,0);
		
		List<FloatBuffer> floats = new ArrayList<FloatBuffer>();
		floats.add(Utilities.asFloatBuffer(vertices));
		floats.add(Utilities.asFloatBuffer(normals));
		floats.add(Utilities.asFloatBuffer(textureUV));
		IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indicies.length);
		indicesBuffer.put(indicies);
		indicesBuffer.flip();
		
		int[] ids = (VAORender.setUp(floats, new int[]{3, 3, 2}, indicesBuffer));
		vaoID = ids[0];
		indiceID = ids[1];
		
		//Render.addJob(new GLJob_SetUpVOA(this, floats, new int[]{3, 3, 2}, indicesBuffer));
	}
	
	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException{
		in.defaultReadObject();
		
		setUpVao();
	}
	
	public void draw(Shader shader){
		//PVM.pushMatrix();
		PVM.translate(translate.x, translate.y, translate.z);
		PVM.scale(scale.x, scale.y, scale.z);
		PVM.rotate(rotate.x, rotate.y, rotate.z);
		PVM.done();
		//PVM.popMatrix();
		VAORender.render(vaoID, indiceID, new int[]{0, 1, 2}, indicies.length);
	}
	
	public void passFloats(float[] vertices, float[] normals, float[] textureUVs, int[] indicies){
		this.vertices = vertices;
		this.normals = normals;
		this.textureUV = textureUVs;
		this.indicies = indicies;
		
	}
	
	public void passMaterial(float[] ambient, float[] diffuse, float[] spectual, float shine){
		this.ambient = ambient;
		this.diffuse = diffuse;
		this.spectual = spectual;
		this.shine = shine;
	}

	/**
	 * @return the ambient
	 */
	public float[] getAmbient() {
		return ambient;
	}

	/**
	 * @return the diffuse
	 */
	public float[] getDiffuse() {
		return diffuse;
	}

	/**
	 * @return the spectual
	 */
	public float[] getSpectual() {
		return spectual;
	}

	/**
	 * @return the shine
	 */
	public float getShine() {
		return shine;
	}
	
	

	/**
	 * @return the vertices
	 */
	public float[] getVertices() {
		return vertices;
	}

	/**
	 * @return the normals
	 */
	public float[] getNormals() {
		return normals;
	}

	/**
	 * @return the textureUV
	 */
	public float[] getTextureUV() {
		return textureUV;
	}

	/**
	 * @return the indicies
	 */
	public int[] getIndicies() {
		return indicies;
	}

	@Override
	public void postResults(GLJob jobInstance, Object... objects) {
		// TODO Auto-generated method stub
		this.vaoID = (int) objects[0];
		this.indiceID = (int) objects[1];
	}
	
	
	
}
