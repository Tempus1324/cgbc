package me.tempus.modelutil;

import java.io.Serializable;

public class Material implements Serializable{
	
	public static final long serialVersionUID = 100L;
	
	private String name;
	private float[] ambient = new float[3];
	private float[] diffuse = new float[3];
	private float[] spectual = new float[3];
	private float shine;
	

	

	public Material(float[] ambient, float[] diffuse, float[] spectual,
			float shine) {
		this.ambient = ambient;
		this.diffuse = diffuse;
		this.spectual = spectual;
		this.shine = shine;
	}
	
	public Material(String name){
		this.name = name;
	}
	/**
	 * @return the ambient
	 */
	public float[] getAmbient() {
		return ambient;
	}
	/**
	 * @param ambient the ambient to set
	 */
	public void setAmbient(float[] ambient) {
		this.ambient = ambient;
	}
	/**
	 * @return the diffuse
	 */
	public float[] getDiffuse() {
		return diffuse;
	}
	/**
	 * @param diffuse the diffuse to set
	 */
	public void setDiffuse(float[] diffuse) {
		this.diffuse = diffuse;
	}
	/**
	 * @return the spectual
	 */
	public float[] getSpectual() {
		return spectual;
	}
	/**
	 * @param spectual the spectual to set
	 */
	public void setSpectual(float[] spectual) {
		this.spectual = spectual;
	}
	/**
	 * @return the shine
	 */
	public float getShine() {
		return shine;
	}
	/**
	 * @param shine the shine to set
	 */
	public void setShine(float shine) {
		this.shine = shine;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}


}
