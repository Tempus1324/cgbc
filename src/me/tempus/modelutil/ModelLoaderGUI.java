package me.tempus.modelutil;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import net.miginfocom.swing.MigLayout;

public class ModelLoaderGUI implements ItemListener {

	private JFrame frame;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	private ModelLoaderTemplate modelLoader;
	private JCheckBox chckbxAdstexturedmodel;
	private JCheckBox chckbxAdsmodel;
	private JFileChooser fileChooser;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModelLoaderGUI window = new ModelLoaderGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ModelLoaderGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setCurrentDirectory(new File("." + File.separator));
		//fileChooser.setFileFilter(new FileNameExtensionFilter());
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new MigLayout("", "[][]", "[][][][][][][][]"));
		
		chckbxAdsmodel = new JCheckBox("ADSModel");
		chckbxAdsmodel.addItemListener(this);
		buttonGroup.add(chckbxAdsmodel);
		frame.getContentPane().add(chckbxAdsmodel, "cell 1 1");
		
		chckbxAdstexturedmodel = new JCheckBox("ADSTexturedModel");
		chckbxAdstexturedmodel.addItemListener(this);
		buttonGroup.add(chckbxAdstexturedmodel);
		frame.getContentPane().add(chckbxAdstexturedmodel, "cell 1 2");
		
		JButton btnLoadModel = new JButton("Load Model");
		btnLoadModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int returnValue = fileChooser.showOpenDialog(frame);
				if(returnValue == JFileChooser.APPROVE_OPTION){
					File file = fileChooser.getSelectedFile();
					try {
						modelLoader.loadMesh(new BufferedReader(new FileReader(file)), file.getParent(), file.getName());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		frame.getContentPane().add(btnLoadModel, "cell 1 7,growx");
	}
	
	public void itemStateChanged(ItemEvent e){
		if(e.getStateChange() == ItemEvent.DESELECTED)
			return;
		Object source = e.getItemSelectable();
		
		if(source == chckbxAdsmodel){
			modelLoader = new ADSModelLoader();
		}else if (source == chckbxAdstexturedmodel){
			
		}
	}

	public JCheckBox getChckbxAdstexturedmodel() {
		return chckbxAdstexturedmodel;
	}
	public JCheckBox getChckbxAdsmodel() {
		return chckbxAdsmodel;
	}
}
