package me.tempus.modelutil;

import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import me.tempus.modelutil.ImpModel;

import me.tempus.util.Face;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class ModelLoader {

	private static ImpModel currentModel;
	
	private static List<Vector3f> currentPosistions = new ArrayList<Vector3f>();
	private static List<Vector3f> currentNormals = new ArrayList<Vector3f>();
	private static List<Vector2f> currentTextureCoords = new ArrayList<Vector2f>();

	private static List<Face> loadedFaces = new ArrayList<Face>();
	private static Face currentFace = null;
	
	private static int[] indices;
	private static Mesh currentMesh = null;

	private static boolean loadingFaces = false;
	private static int amountOfFaces = 0;
	private static int faceIndex = 0;

	private static List<Mesh> meshes = new ArrayList<Mesh>();

	private static HashMap<String, Material> materials = new HashMap();

	private static int i;

	private static int debug_nuOfFacePush;

	public static ImpModel loadMesh(String folderPath, String fileName) throws IOException{

		BufferedReader reader = new BufferedReader(new FileReader(folderPath + File.separatorChar + fileName));
		BufferedReader mtlReader;
		currentModel = new ImpModel();

		String currentLine;
		Material currentMaterial;
		int i = 0;

		while((currentLine = reader.readLine()) != null){
			if(currentLine.startsWith("mtllib ")){
				String[] values = currentLine.split(" ");
				mtlReader = new BufferedReader(new FileReader(folderPath + File.separatorChar + values[1]));
				loadMaterialLibrary(mtlReader);
			}else if(currentLine.startsWith("usemtl ")){
				/**
				 * TODO Read below
				 *  Pull down material
				 *  Grab float data out of material and push it to the current mesh
				 *  Push vertices data and load the indices list with the faces
				 */
				pushFaces();
				String[] values = currentLine.split(" ");
				if(values.length >= 2){
					currentMaterial = materials.get(values[1]);
				}else{
					currentMaterial = new Material("");
				}
				//System.err.println("Name: " + values[1]);
				i++;
				currentFace = new Face(currentMaterial);
			}else{
				/**
				 * TODO Read below
				 * If have no other flags
				 * Pass line to readLine to load the data, should be a compontent of the vertex 
				 */
				readLine(currentLine);
			}
		}
		if(loadingFaces){
			loadingFaces = false;
			pushFaces();
		}
		System.out.println("Number of materials: " + i);
		System.out.println("Number of Faces: " + loadedFaces.size());
		generateMeshes();
		System.out.println("Number of meshes: " + meshes.size());
		reader.close();
		currentModel.setMeshes(meshes);
		return currentModel;
	}

	private static void loadMaterialLibrary(BufferedReader reader) throws IOException{

		String currentLine;
		Material currentMaterial = null;
		while((currentLine = reader.readLine()) != null){
			/**
			 * TODO Read below
			 * Read through material file and load the different materials
			 * Push materials to HashMap of materials and there names
			 */
			if(currentLine.startsWith("newmtl ")){
				String[] values = currentLine.split(" ");
				if(values.length == 2){
					currentMaterial = new Material(values[1]);
				}else{
					currentMaterial = new Material("");
				}
				if(currentMaterial != null){
					materials.put(currentMaterial.getName(), currentMaterial);
				}
			}else if(currentLine.startsWith("Ka ")){
				String[] values = currentLine.split(" ");
				float r = Float.valueOf(values[1]);
				float g = Float.valueOf(values[2]);
				float b = Float.valueOf(values[3]);
				currentMaterial.setAmbient(new float[]{r, g, b});
			}else if(currentLine.startsWith("Ks ")){
				String[] values = currentLine.split(" ");
				float r = Float.valueOf(values[1]);
				float g = Float.valueOf(values[2]);
				float b = Float.valueOf(values[3]);
				currentMaterial.setSpectual(new float[]{r, g, b});
			}else if(currentLine.startsWith("Kd ")){
				String[] values = currentLine.split(" ");
				float r = Float.valueOf(values[1]);
				float g = Float.valueOf(values[2]);
				float b = Float.valueOf(values[3]);
				currentMaterial.setDiffuse(new float[]{r, g, b});
			}else if(currentLine.startsWith("Ns ")){
				String[] values = currentLine.split(" ");
				float shine = Float.valueOf(values[1]);
				currentMaterial.setShine(shine);
			}else if(currentLine.startsWith("map_Kd ")){
				String[] values = currentLine.split(" ");
				
			}
		}
		reader.close();

	}

	private static void generateMeshes(){
		for(Face currentFace: loadedFaces){
			Mesh newMesh = loadIndices(currentFace.getLoadedFaces(), currentFace.getFaces());
			Material meshMaterial = currentFace.getMaterial();
			newMesh.passMaterial(meshMaterial.getAmbient(), meshMaterial.getDiffuse(), meshMaterial.getSpectual(), meshMaterial.getShine());
			meshes.add(newMesh);
		}
	}
	
	private static void pushFaces(){

		if(!(loadedFaces.contains(currentFace)) && currentFace != null){
			loadedFaces.add(currentFace);
		}
	}
	
	private static void readLine(String line){
		if(line.startsWith("v ")){
			String[] values = line.split(" ");
			float x = Float.valueOf(values[1]);
			float y = Float.valueOf(values[2]);
			float z = Float.valueOf(values[3]);
			Vector3f pos = new Vector3f(x, y, z);
			currentPosistions.add(pos);
		}else if(line.startsWith("vn ")){
			String[] values = line.split(" ");
			float x = Float.valueOf(values[1]);
			float y = Float.valueOf(values[2]);
			float z = Float.valueOf(values[3]);
			Vector3f normal = new Vector3f(x, y, z);
			currentNormals.add(normal);
		}else if(line.startsWith("vt ")){
			String[] values = line.split(" ");
			float x = Float.valueOf(values[1]);
			float y = Float.valueOf(values[2]);
			Vector2f textureCoord = new Vector2f(x, y);
			currentTextureCoords.add(textureCoord);
		}else if(line.startsWith("f ")){
			String[] values = line.split(" ");
			loadingFaces = true;
			currentFace.loadFace(values[1]);
			currentFace.loadFace(values[2]);
			currentFace.loadFace(values[3]);			
		}else if(!(line.startsWith("f ")) && loadingFaces){
			// Stop loading faces, reached end of the current Mesh
			loadingFaces = false;
			//loadIndices(currentFaces.size(), currentMesh);
			debug_nuOfFacePush++;
			System.out.println("Number of face pushes: " + debug_nuOfFacePush);
			pushFaces();
		}
	}

	private static Mesh loadIndices(HashMap<String, Integer> faces, List<String> currentFaces){
		Mesh currentMesh = new Mesh();
		indices = new int[currentFaces.size()];
		int amountOfLoadedFaces = faces.size();
		float[] posistions = new float[amountOfLoadedFaces * 3];
		float[] normals = new float[amountOfLoadedFaces * 3];
		float[] textureCoords = new float[amountOfLoadedFaces * 2];

		String[] facesArray = (faces.keySet()).toArray(new String[amountOfLoadedFaces]);

		for(int i = 0; i < amountOfLoadedFaces; i++){
			String faceString = facesArray[i];
			String[] faceValues = faceString.split("/");
			int currentPos = faces.get(faceString);

			Vector3f posistion = currentPosistions.get(Integer.parseInt(faceValues[0]) -1);
			posistions[(currentPos * 3)] = posistion.x;
			posistions[(currentPos * 3) +1] = posistion.y;
			posistions[(currentPos * 3) +2] = posistion.z;

			if(!(faceValues[1].equalsIgnoreCase(""))){
			Vector2f textureCoord = currentTextureCoords.get(Integer.parseInt(faceValues[1]) -1);
			textureCoords[(currentPos *2)] = textureCoord.x;
			textureCoords[(currentPos *2) +1] = textureCoord.y;
			}

			Vector3f normal = currentNormals.get(Integer.parseInt(faceValues[2]) -1);
			normals[(currentPos *3)] = normal.x;
			normals[(currentPos *3) +1] = normal.y;
			normals[(currentPos *3) +2] = normal.z;

		}

		for(int i = 0; i < currentFaces.size(); i++){
			int index = faces.get(currentFaces.get(i));
			indices[i] = index;
		}

		currentMesh.passFloats(posistions, normals, textureCoords, indices);
		return currentMesh;
	}

	public static ImpModel deserialModel(String modelPath){
		Object model = null;


		try {
			FileInputStream fis = new FileInputStream(modelPath); 
			ObjectInputStream ois = new ObjectInputStream(fis);
			model = ois.readObject();
			ois.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}


		return (ImpModel) model;
	}

	/**
	 * Serialize a model
	 */
	public static void main(String[] args){
		ImpModel model;

		System.out.println("Please enter the name of the model file to be loaded (*.obj): ");
		Scanner scanner = new Scanner(System.in);
		String[] values = scanner.nextLine().split(" ");
		String path = values[1];
		String folder = values[0];
		
		scanner.close();

		try {
			model = loadMesh(folder, path);
			System.out.println("Models" + File.separator + path);
			FileOutputStream fos = new FileOutputStream("Models" + File.separator + path); 
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(model);
			oos.flush();
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	public static ImpModel loadModel(File modelFile) throws IOException{

		BufferedReader reader = new BufferedReader(new FileReader(modelFile));
		BufferedReader readerMtl = null;
		ImpModel model = new ImpModel();
		String currentLine;

		/**
		 * TODO Add material (lighting) 
		 
		String materialName = null;

		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector3f> normals = new ArrayList<Vector3f>();
		List<Vector2f> textureUVs = new ArrayList<Vector2f>();
		List<String> faces = new ArrayList<String>();

		HashMap<String, Integer> loadedVertices = new HashMap<String, Integer>();

		List<Mesh> meshes = new ArrayList<Mesh>();
		Mesh currentMesh;

		while((currentLine = reader.readLine()) != null){			

			if(currentLine.startsWith("mtllib ")){
				String[] v = currentLine.split(" ");
				readerMtl = new BufferedReader(new FileReader("Data" + File.separator + v[1]));
			}else if(currentLine.startsWith("usemtl ")){
				String[] v = currentLine.split(" ");
				materialName = v[1];

				currentMesh = new Mesh();
			}else if(currentLine.startsWith("vt ")){
				String[] v = currentLine.split(" ");
				float x = Float.valueOf(v[1]);
				float y = Float.valueOf(v[2]);
				textureUVs.add(new Vector2f(x, y));
			}else if(currentLine.startsWith("v ")){
				String[] v = currentLine.split(" ");
				float x = Float.valueOf(v[1]);
				float y = Float.valueOf(v[2]);
				float z = Float.valueOf(v[3]);
				vertices.add(new Vector3f(x, y, z));
			}else if(currentLine.startsWith("vn ")){
				String[] v = currentLine.split(" ");
				float x = Float.valueOf(v[1]);
				float y = Float.valueOf(v[2]);
				float z = Float.valueOf(v[3]);
				normals.add(new Vector3f(x,y,z));
			}else if(currentLine.startsWith("f ")){
				try{
					String[] v = currentLine.split(" ");
					//System.out.println(v[0] + " "  + v[1] + " "  + v[2] + " " + v[3]);
					//String[] c1 = v[1].split("/");
					//String[] c2 = v[2].split("/");
					//String[] c3 = v[3].split("/");

					/*
					Vector3f vertextIndices = new Vector3f(
							Float.valueOf(c1[0]), Float.valueOf(c2[0]), Float.valueOf(c3[0]));
					Vector3f textureIndices = new Vector3f(
							Float.valueOf(c1[1]), Float.valueOf(c2[1]), Float.valueOf(c3[1]));
					Vector3f normalIndices = new Vector3f(
							Float.valueOf(c1[2]), Float.valueOf(c2[2]), Float.valueOf(c3[2]));
					//faces.add(new Face(vertextIndices, textureIndices, normalIndices, materialName));

					 
					faces.add(v[1]);
					faces.add(v[2]);
					faces.add(v[3]);

				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}

		if(readerMtl != null){
			while((currentLine = readerMtl.readLine()) != null){
				float[] ambient = new float[3];
				float[] diffuse = new float[3];
				float[] spectual = new float[3];
				float shine = 0;
				String materialName1 = null;

				if(currentLine.startsWith("Ka ")){
					String[] v = currentLine.split(" ");
					ambient[0] = Float.parseFloat(v[1]);
					ambient[1] = Float.parseFloat(v[2]);
					ambient[2] = Float.parseFloat(v[3]);
				}else if(currentLine.startsWith("Kd ")){
					String[] v = currentLine.split(" ");
					diffuse[0] = Float.parseFloat(v[1]);
					diffuse[1] = Float.parseFloat(v[2]);
					diffuse[2] = Float.parseFloat(v[3]);
				}else if(currentLine.startsWith("Ks ")){
					String[] v = currentLine.split(" ");
					spectual[0] = Float.parseFloat(v[1]);
					spectual[1] = Float.parseFloat(v[2]);
					spectual[2] = Float.parseFloat(v[3]);
				}else if(currentLine.startsWith("Ns ")){
					String[] v = currentLine.split(" ");
					shine = Float.parseFloat(v[1]);
				}else if(currentLine.startsWith("newmtl ")){
					String[] v= currentLine.split(" ");
					//materialName1 = v[1];
				}

				model.addMaterial(materialName1, new Material(ambient, diffuse, spectual, shine));
			}
		}
		reader.close();
		if(readerMtl != null){
			readerMtl.close();
		}

		System.out.println("Amount of faces: " + faces.size());
		System.out.println("Amount of UVs: " + textureUVs.size());
		System.out.println("Amount of normals: " + normals.size());
		//model.setFloatSizes(vertices.size() * 3 * faces.size(), normals.size() * 3 * faces.size(), textureUVs.size() * 2 * faces.size());

		//Change to lists without set size, due to vertices using the same piece of data but different with another piece of data. Eg 1/1/1 and 2/1/3

		int[] indices = new int[faces.size()];

		int i = 0;
		int faceNu = 0;

		for (String face : faces) {

			//String[] faceSplit = face.split("/");
			//StringBuilder faceString = new StringBuilder();
			//faceString.append(faceSplit[0]).append(faceSplit[1]).append(faceSplit[2]);
			//System.out.println("Contains face: " + faceString.toString());
			if(loadedVertices.containsKey(face)){
				int vertexNu = loadedVertices.get(face);
				//System.out.println("VertexNu: " + vertexNu);
				indices[faceNu] = vertexNu;
				//System.out.println("VertexNu as byte: " + (byte)vertexNu);
			}else{
				//System.out.println("i: " + i);
				//System.out.println("Face: " + face[0] + face[1] + face[2]);

				loadedVertices.put(face, i);
				indices[faceNu] = i;
				//System.out.println("i as byte: " + (byte)i);
				i++;

			}
			faceNu++;

		}
		float[] posistions = new float[i *3];
		float[] normal = new float[i *3];
		float[] textureUV = new float[i *2];

		int currentPos = 0;


		for(String index : loadedVertices.keySet()){

			String[] vertex = index.split("/");
			//System.out.println("Index 0: " + index.charAt(0));
			Vector3f pos = vertices.get(Integer.parseInt(vertex[0]) -1);
			currentPos = loadedVertices.get(index);
			//System.out.println("CurrentPos: " + currentPos);
			posistions[currentPos *3] = pos.x;
			posistions[(currentPos *3) +1] = pos.y;
			posistions[(currentPos *3) +2] = pos.z;			

			pos = normals.get(Integer.parseInt(vertex[2]) -1);
			normal[currentPos *3] = pos.x;
			normal[(currentPos *3) +1] = pos.y;
			normal[(currentPos *3) +2] = pos.z;

			if(textureUVs.size() >= 1){
				Vector2f tpos = textureUVs.get(Integer.parseInt(vertex[1]) -1);
				textureUV[currentPos *2] = tpos.x;
				textureUV[(currentPos *2) +1] = tpos.y;
			}
		}

		model.passFloats(posistions, normal, textureUV, indices);
		return model;

	}
*/
}
