package me.tempus.modelutil;

public class TexturedMaterial extends Material {

	private int textureIndex;
	
	public TexturedMaterial(float[] ambient, float[] diffuse, float[] spectual,
			float shine) {
		super(ambient, diffuse, spectual, shine);
		// TODO Auto-generated constructor stub
	}

	public TexturedMaterial(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public void setTextureIndex(int textureIndex){
		this.textureIndex = textureIndex;
	}
	
	public int getTextureIndex(){
		return textureIndex;
	}

}
