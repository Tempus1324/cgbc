/**
 * 
 */
package me.tempus.modelutil;

import me.tempus.shader.ADSTAShader;


/**
 * @author Chris
 *
 */
public class ADSTAModel extends ImpModel {
	
	private int textureIndex;
	
	@Override
	public void passData(float[] ambient, float[] diffuse, float[] spectual, float shine){
		((ADSTAShader) shader).passUniforms(ambient, diffuse, spectual, shine, textureIndex);		
	}
	
	public static ADSTAModel phaseToThis(ImpModel model){
		ADSTAModel newModel = new ADSTAModel();
		newModel.setMeshes(model.getMeshes());
		newModel.setRotate(model.getRotate());
		newModel.setScale(model.getScale());
		newModel.setTranslate(model.getTranslate());
		return newModel;
	}
	
	public void setTextureIndex(int textureIndex){
		this.textureIndex = textureIndex;
	}
	
}
