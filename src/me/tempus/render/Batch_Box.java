package me.tempus.render;

public class Batch_Box implements Renderable {

	
	public static void renderBoxes(){
		/**
		 * Loop through a list of boxes to be rendered.
		 */
	}
	
	private static void renderBox(){
		/**
		 * The method for rendering a specific box
		 * Will need all it's data.
		 * Vertices, colours, texture coords and normals
		 * Each box will have it's own texture
		 * Shader should use directional lighting
		 */
	}
	
	public static void addBox(Box box){
		/**
		 * Add a box to the list of of boxes to draw each frame
		 */
	}

	@Override
	public void preDraw() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postDraw() {
		// TODO Auto-generated method stub
		
	}
}
