package me.tempus.render;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import me.tempus.collision.AABB;
import me.tempus.collision.AABV;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Vector3f;

public class Box {

	/**
	 * An 8 vertex box
	 */
	
	private int[] indices;
	private FloatBuffer vertices;
	private FloatBuffer textureCoords;
	private FloatBuffer normals;
	private FloatBuffer colours;
	
	
	
	/**
	 * Creates a renderable box with a bounding volume
	 * @param aabb
	 */
	public Box(AABV aabb, int[] colours){
		Object[] data = AABB.getRenderable(aabb);
		//vertices = Util.asFloatBuffer4((float[]) data[0]);
		indices = (int[]) data[1];
		for(float f : (float[])data[0]){
			System.out.println("Vertex: " + f);
		}
	}
	
	/**
	 * 
	 * @param centre
	 * @param extents
	 * @return
	 */
	public static Box generateBoxFromPoint(Vector3f centre, Vector3f extents){
		//TODO Write this
		/**
		 * It should use a static box points created from an obj model file, then transformation and scale based on input
		 * Or, just use the static data from model file and use inputs as positions
		 */
		final FloatBuffer vertices = BufferUtils.createFloatBuffer(72);
		final FloatBuffer normals = BufferUtils.createFloatBuffer(72);
		final FloatBuffer textureCords = BufferUtils.createFloatBuffer(48);
		final IntBuffer indices = BufferUtils.createIntBuffer(36);
		vertices.put( //The vertices for the box
				new float[]{-5.0f, -5.0f, 5.0f, -5.0f, -5.0f, -5.0f, 5.0f, -5.0f, -5.0f, 5.0f, -5.0f, 5.0f, -5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, -5.0f, -5.0f, 5.0f, -5.0f, -5.0f, -5.0f, 5.0f, 5.0f, -5.0f, 5.0f, 5.0f, 5.0f, 5.0f, -5.0f, 5.0f, 5.0f, 5.0f, -5.0f, 5.0f, 5.0f, -5.0f, -5.0f, 5.0f, 5.0f, -5.0f, 5.0f, 5.0f, 5.0f, 5.0f, -5.0f, -5.0f, -5.0f, -5.0f, -5.0f, -5.0f, 5.0f, -5.0f, 5.0f, 5.0f, -5.0f, -5.0f, -5.0f, -5.0f, -5.0f, -5.0f, 5.0f, -5.0f, 5.0f, 5.0f, -5.0f, 5.0f, -5.0f});
		normals.put( //Normals
				new float[]{0.0f, -1.0f, -0.0f, 0.0f, -1.0f, -0.0f, 0.0f, -1.0f, -0.0f, 0.0f, -1.0f, -0.0f, 0.0f, 1.0f, -0.0f, 0.0f, 1.0f, -0.0f, 0.0f, 1.0f, -0.0f, 0.0f, 1.0f, -0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, -0.0f, 1.0f, 0.0f, -0.0f, 1.0f, 0.0f, -0.0f, 1.0f, 0.0f, -0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, -1.0f, 0.0f, -0.0f, -1.0f, 0.0f, -0.0f, -1.0f, 0.0f, -0.0f, -1.0f, 0.0f, -0.0f});
		textureCords.put( //Texture Cords
				new float[]{1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f});
		indices.put(
				new int[]{0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4, 8, 9, 10, 10, 11, 8, 12, 13, 14, 14, 15, 12, 16, 17, 18, 18, 19, 16, 20, 21, 22, 22, 23, 20});
		return null;
	}
	
}
