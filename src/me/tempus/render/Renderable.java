package me.tempus.render;


/**
 * @author Chris
 * Interface for anything that will be drawn to the screen.
 */
public interface Renderable {
	
	public abstract void preDraw();
	
	public abstract void draw();
	
	public abstract void postDraw();
}
