@echo off
color 0A
title "Add and Commit files"

git init

:addstart
set /p adding="Which files would you like to add (Dir): "
git add "%adding%"
set /p choice="Would you like to add more? (y/n): "
if "%choice%" == "y" goto addstart


echo.
echo A good commit message has the following format:
echo.
echo - First line: Describe in one sentence what you did.
echo - Second line: Blank
echo - Remaining lines: Describe why this change is good.
set /p commit="Please supply a commit message: "
git commit -m "%commit%"
echo Prepare to enter your password
git push
pause